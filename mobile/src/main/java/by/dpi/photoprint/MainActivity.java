package by.dpi.photoprint;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import by.dpi.photoprint.RjTooltip.Tooltip;
import by.dpi.photoprint.model.*;
import com.google.firebase.database.*;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.util.*;

public class MainActivity extends AppCompatActivity {

	PublishSubject<PriceMap> priceMapPublishSubject;
	boolean dataLoaded = false;
	boolean menuLoaded = false;
	List<by.dpi.photoprint.model.MenuItem> menuItemList;
	int tutorialStep = 0;
	private PriceMapSingleton priceMapSingleton;
	private DatabaseReference database;
	private boolean isExpandPrintLayout;
	private boolean isShowDialog;
	private boolean carouselItemClick = true;
	private boolean tutorial = false;
	private int carouselPosition;
	private long appLaunchDate;
	private List<Order> orders;
	private SharedPreferencesService sharedPreferencesService;
	private TextView textViewResult;
	private TextView carouselTextView;
	private TextView lastResultTextView;
	private TextView deliveryTextView;
	private Button printButton;
	private Button cancelPrintButton;
	private RadioGroup radioGroup;
	private ArrayList<RadioButton> radioButtons;
	private Timer carouselTimer;
	private Toolbar toolbar;
	private ConstraintLayout loadingPanel;
	private ConstraintLayout printLayout;
	private ConstraintLayout dialogLayout;
	private ConstraintLayout blackoutLayout;
	private ConstraintLayout.LayoutParams printButtonLayoutParams;
	private OrderAdapterPrint orderAdapterPrint;
	private RecyclerView carouselRecyclerView;
	private RecyclerView orderRecyclerView;
	private int position;
	private DialogLayout dialog;
	private ValueEventListener postListener;
	private ValueEventListener menuListener;
	private Disposable pricesMapDisposable;
	private ConstraintLayout tutorialLayout;
	private ImageView orderImageView;
	private Button deleteOrderButton;
	private Button addOrderButton;
	private Spinner orderSizeSpinner;
	private Spinner orderQualitySpinner;
	private Spinner orderQuantitySpinner;
	private ProgressBar loading;
	private Menu _menu = null;

	private int dp(int pixels) {
		return Math.round(pixels * Resources.getSystem().getDisplayMetrics().density);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		priceMapSingleton = PriceMapSingleton.getInstance();

		database = FirebaseDatabase.getInstance().getReference();

		sharedPreferencesService = new SharedPreferencesService(getApplicationContext());

		textViewResult = findViewById(R.id.textViewResult);
		carouselTextView = findViewById(R.id.carouselTextView);
		lastResultTextView = findViewById(R.id.lastResultTextView);
		deliveryTextView = findViewById(R.id.deliveryTextView);

		printButton = findViewById(R.id.printButton);
		cancelPrintButton = findViewById(R.id.cancelPrintButton);

		radioGroup = findViewById(R.id.carouselRadioGroup);

		radioButtons = new ArrayList<>();
		radioButtons.add(findViewById(R.id.carouselRadioButtonOne));
		radioButtons.add(findViewById(R.id.carouselRadioButtonTwo));
		radioButtons.add(findViewById(R.id.carouselRadioButtonThree));

		carouselTimer = new Timer();

		toolbar = findViewById(R.id.toolbar);

		loadingPanel = findViewById(R.id.loadingPanel);
		printLayout = findViewById(R.id.printLayout);
		dialogLayout = findViewById(R.id.dialogLayout);
		blackoutLayout = findViewById(R.id.layout_blackout);

		carouselRecyclerView = findViewById(R.id.carouselRecyclerView);
		orderRecyclerView = findViewById(R.id.orderRecyclerView);

		orderAdapterPrint = new OrderAdapterPrint(this);

		tutorialLayout = findViewById(R.id.tutorialLayout);
		orderImageView = findViewById(R.id.orderImageView);
		deleteOrderButton = findViewById(R.id.deleteOrderButton);
		addOrderButton = findViewById(R.id.addOrderButton);

		dialog = new DialogLayout(this);

		loading = findViewById(R.id.loading);

		tutorial = true;

		setParameters();
		setListeners();


		publishListener();
	}

	private void listenDatabase() {
		postListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				GenericTypeIndicator<ArrayList<PriceList>> t = new GenericTypeIndicator<ArrayList<PriceList>>() {
				};
				List<PriceList> priceList = dataSnapshot.getValue(t);
				if (priceList != null)
					priceMapPublishSubject.onNext(priceMapSingleton.firebaseConvert(priceList));
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {
				Log.w("err", "loadPost:onCancelled", databaseError.toException());
			}
		};

		menuListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				GenericTypeIndicator<ArrayList<by.dpi.photoprint.model.MenuItem>> t = new GenericTypeIndicator<ArrayList<by.dpi.photoprint.model.MenuItem>>() {
				};
				menuItemList = dataSnapshot.getValue(t);
				menuLoaded = true;
				if (_menu != null)
					onCreateOptionsMenu(_menu);
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {
				Log.w("err", "loadPost:onCancelled", databaseError.toException());
			}
		};

		database.child("Prices").addValueEventListener(postListener);
		database.child("Menu").addValueEventListener(menuListener);
	}

	private void publishListener() {
		priceMapPublishSubject = PublishSubject.create();

		pricesMapDisposable = priceMapPublishSubject.subscribe(
			priceMap -> {
				priceMapSingleton.setPriceMap(priceMap);
				orders = MainViewModel.getInstance().getOrders();
				if (orders == null) {
					orders = new ArrayList<>();
					tutorialConfigure();
				} else {
					orderAdapterPrint.onAddNotify(orders);
				}
			}, Throwable::printStackTrace);
	}

	private void setParameters() {

		if (MainViewModel.getInstance() == null)
			new ViewModelProvider(this).get(MainViewModel.class);

		carouselTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				carouselPosition++;
				if (carouselPosition > 2)
					carouselPosition = 0;
				runOnUiThread(() -> selectRadioButton(carouselPosition));
			}
		}, 0, 3000);

		setSupportActionBar(toolbar);

		LinearLayoutManager carouselLayoutManager = new LinearLayoutManager(getBaseContext());
		carouselLayoutManager.setOrientation(RecyclerView.HORIZONTAL);
		CarouselRecyclerAdapter carouselRecyclerAdapter = new CarouselRecyclerAdapter(this);
		carouselRecyclerView.setLayoutManager(carouselLayoutManager);
		carouselRecyclerView.setAdapter(carouselRecyclerAdapter);

		LinearLayoutManager ordersLayoutManager = new LinearLayoutManager(getBaseContext());
		orderRecyclerView.setLayoutManager(ordersLayoutManager);
		orderRecyclerView.setAdapter(orderAdapterPrint);

		printLayout.setElevation(dp(20));
		dialogLayout.setElevation(dp(40));

		orderImageView.setVisibility(View.GONE);
		deleteOrderButton.setVisibility(View.GONE);
		addOrderButton.setVisibility(View.GONE);
	}

	@SuppressLint("ClickableViewAccessibility")
	private void setListeners() {
		printButton.setOnClickListener(v -> {
			if (!isExpandPrintLayout) expandPrintLayout();
			else {
				Intent intent = new Intent(getBaseContext(), ProgressActivity.class);
				startActivityForResult(intent, RequestCodes.ACTIVITY_PROGRESS.getRequestCode());
			}
		});

		cancelPrintButton.setOnClickListener(v -> expandPrintLayout());

		blackoutLayout.setOnClickListener(v -> {
			if (isExpandPrintLayout) expandPrintLayout();
			else showDialogLayout();
		});

		radioGroup.setOnCheckedChangeListener((group, checkedId) -> {
			RadioButton radioButton = findViewById(checkedId);
			carouselRecyclerView.scrollToPosition(radioButtons.lastIndexOf(radioButton));
		});

		carouselRecyclerView.setOnTouchListener((v, event) -> {
			switch (event.getAction()) {
				case (MotionEvent.ACTION_DOWN):
				case (MotionEvent.ACTION_MOVE):
					carouselItemClick = true;
					break;
				case (MotionEvent.ACTION_UP):
					carouselItemClick = false;
					break;
			}
			return false;
		});
	}

	public void onCarouselItemClick(int position) {
		if (carouselItemClick)
			selectRadioButton(position);
	}

	public void selectRadioButton(int position) {
		radioGroup.check(radioButtons.get(position).getId());
	}

	private void addNewOrder() {
		Order order = new Order();
		order.setNum(orders.size());
		orders.add(order);
		orderAdapterPrint.onAddNotify(orders);
	}

	@Override
	protected void onStart() {
		super.onStart();

		if (sharedPreferencesService.isPhoneNumberNull())
			startActivityForResult(new Intent(this, LoginActivity.class), RequestCodes.LOGIN_CODE.getRequestCode());
		else listenDatabase();

		appLaunchDate = sharedPreferencesService.getLastAppLaunch();
		sharedPreferencesService.setLastAppLaunch(Calendar.getInstance().getTime().getTime());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		_menu = menu;
		if (menuLoaded) {
			_menu.clear();

			int i = 0;
			for (by.dpi.photoprint.model.MenuItem item : menuItemList) {
				_menu.add(0, Menu.FIRST + i, Menu.NONE, item.getName_ru());
				i++;
			}

			getMenuInflater().inflate(R.menu.menu_main, _menu);
		}
		return super.onCreateOptionsMenu(_menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		Intent intent = new Intent(this, WebActivity.class);
		int id = item.getItemId();
		if (id - Menu.FIRST < menuItemList.size() && id - Menu.FIRST >= 0) {
			by.dpi.photoprint.model.MenuItem menuItem = menuItemList.get(id - Menu.FIRST);
			String content = "<!DOCTYPE html>\n" +
				"<html lang=\"en\">\n" +
				"    <head>\n" +
				"        <meta charset=\"UTF-8\">\n" +
				"        <title>Delivery</title>\n" +
				"    </head>\n" +
				"    <body>\n" +
				menuItem.getHtml() +
				"    </body>\n" +
				"</html>";

			intent.putExtra("content", content);
			startActivityForResult(intent, RequestCodes.PRICE_SHOW.getRequestCode());

		}


		switch (id) {
			case (R.id.ver_app):
				PackageInfo packageInfo = null;
				try {
					packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
				} catch (PackageManager.NameNotFoundException e) {
					e.printStackTrace();
				}

				assert packageInfo != null;
				String ver = packageInfo.versionName;
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Версия программы:");
				builder.setMessage(ver);
				builder.setPositiveButton("Ок", (dialog, which) -> {
				});
				builder.show();
				break;
			case (R.id.share_menuitem):
				startShareActivity();
				break;
			case (R.id.rate_menuitem):
				startRateActivity();
				break;
			case (R.id.changenumber_menuitem):
				startActivity(new Intent(this, LoginActivity.class).putExtra("change_number", true));
				break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		super.onResume();

		isExpandPrintLayout = false;

		dialogLayout.setAlpha(0f);
		dialogLayout.setVisibility(View.INVISIBLE);

		textViewResult.setAlpha(1f);
		deliveryTextView.setAlpha(0f);
		lastResultTextView.setAlpha(0f);

		setUiComponentsVisibility(View.GONE);

		if (!dataLoaded) {
			loadingPanel.setVisibility(View.VISIBLE);
			loading.setVisibility(View.VISIBLE);
			tutorialLayout.setVisibility(View.GONE);
		} else {
			if (tutorial) {
				orderRecyclerView.setVisibility(View.GONE);
				loading.setVisibility(View.GONE);
				tutorialLayout.setVisibility(View.VISIBLE);
			} else {
				orderRecyclerView.setVisibility(View.VISIBLE);
				loadingPanel.setVisibility(View.GONE);
			}
		}

		if (orders != null && orders.size() > 1) {
			setUiComponentsVisibility(View.VISIBLE);
			setCarouselVisibility(View.GONE);
		} else
			setCarouselVisibility(View.VISIBLE);

		if (orders == null || orders.size() < 2)
			textViewResult.setText("Итого: 0.00");
		else {
			Log.w("Order size:", String.valueOf(orders.size()));
			textViewResult.setText("Итого: " + String.format("%.2f", Math.max(ordersGetResult(), 3f)));
			lastResultTextView.setText("ИТОГО заказ: " + String.format("%.2f", Math.max(ordersGetResult(), 3f)) + " руб");
		}

		printButtonLayoutParams = (ConstraintLayout.LayoutParams) printButton.getLayoutParams();
		printButtonLayoutParams.topMargin = printButtonLayoutParams.bottomMargin;
		printButton.setLayoutParams(printButtonLayoutParams);

		blackoutLayout.setVisibility(View.GONE);

		MainViewModel.getInstance().setOrders(orders);
	}

	private void setUiComponentsVisibility(int visibility) {
		textViewResult.setVisibility(visibility);
		printButton.setVisibility(visibility);
		printLayout.setVisibility(visibility);
		cancelPrintButton.setVisibility(visibility == View.VISIBLE ? View.INVISIBLE : visibility);
		deliveryTextView.setVisibility(visibility);
		lastResultTextView.setVisibility(visibility);
	}

	private void tutorialConfigure() {

		long date = Calendar.getInstance().getTime().getTime();
		if (appLaunchDate < date - 7776000000L /* 3 mon */) {
			sharedPreferencesService.clearTooltipShowCount();
		}

		final boolean tooltip = sharedPreferencesService.getTooltipShowCount() < 3;

		orderSizeSpinner = findViewById(R.id.orderSizeSpinner);
		orderQualitySpinner = findViewById(R.id.orderQualitySpinner);
		orderQuantitySpinner = findViewById(R.id.orderQuantitySpinner);

		List<String> sizes;
		List<String> qualities;
		ArrayAdapter<String> sizeAdapter;
		ArrayAdapter<String> qualityAdapter;

		sizes = new ArrayList<>();
		qualities = new ArrayList<>();

		sizes.add("—");
		sizes.addAll(priceMapSingleton.getPriceMap().getSizes());
		qualities.add("—");
		qualities.addAll(priceMapSingleton.getPriceMap().getQualities());

		sizeAdapter = new ArrayAdapter<>(this, R.layout.spinersize, sizes);
		sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		orderSizeSpinner.setAdapter(sizeAdapter);

		qualityAdapter = new ArrayAdapter<>(this, R.layout.spinersize, qualities);
		qualityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		orderQualitySpinner.setAdapter(qualityAdapter);

		ArrayAdapter<CharSequence> quantityAdapter = ArrayAdapter.createFromResource(this,
			R.array.quantity_array2, R.layout.spinersize);
		quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		orderQuantitySpinner.setAdapter(quantityAdapter);

		orderSizeSpinner.setEnabled(false);
		orderQualitySpinner.setEnabled(false);
		orderQuantitySpinner.setEnabled(false);

		orderSizeSpinner.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_spinner_disabled));
		orderQualitySpinner.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_spinner_disabled));
		orderQuantitySpinner.setBackground(ContextCompat.getDrawable(this, R.drawable.shape_spinner_disabled));

		orderSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				int selectedPos = orderQualitySpinner.getSelectedItemPosition();
				String selectedQuality = qualities.get(orderQualitySpinner.getSelectedItemPosition());

				qualities.clear();
				qualities.add("—");
				qualities.addAll(priceMapSingleton.getPriceMap().getQualitiesBySize(sizes.get(position)));

				if (qualities.contains(selectedQuality))
					orderQualitySpinner.setSelection(qualities.indexOf(selectedQuality));
				else if (selectedPos < qualities.size())
					orderQualitySpinner.setSelection(selectedPos);
				else orderQualitySpinner.setSelection(qualities.size() - 1);

				qualityAdapter.notifyDataSetChanged();

				if (tutorialStep == 1) {
					orderQualitySpinner.setEnabled(true);
					orderQualitySpinner.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.shape_spinner));
					if (tooltip) {
						Tooltip tooltip = Tooltip.make(MainActivity.this, orderQualitySpinner, "Подсказка");
						tooltip.show();
					}
					tutorialStep++;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		orderQualitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (tutorialStep == 2) {
					orderQuantitySpinner.setEnabled(true);
					orderQuantitySpinner.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.shape_spinner));
					if (tooltip) {
						Tooltip tooltip = Tooltip.make(MainActivity.this, orderQuantitySpinner, "Подсказка");
						tooltip.show();
					}
					tutorialStep++;
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		orderQuantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				if (tutorialStep == 0) {
					orderSizeSpinner.setEnabled(true);
					orderSizeSpinner.setBackground(ContextCompat.getDrawable(MainActivity.this, R.drawable.shape_spinner));
					if (tooltip) {
						Tooltip tooltip = Tooltip.make(MainActivity.this, orderSizeSpinner, "Подсказка");
						tooltip.show();
					}
					tutorialStep++;
				} else if (tutorialStep == 3) {
					addOrderButton.setVisibility(View.VISIBLE);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});

		addOrderButton.setOnClickListener(v -> {
			Order order = new Order();
			order.setNum(orders.size());

			int sizePos = orderSizeSpinner.getSelectedItemPosition();
			int qualityPos = orderQualitySpinner.getSelectedItemPosition();

			float price = priceMapSingleton.getPriceMap().getPriceBySelection(qualityPos - 1, sizePos - 1);

			order.setParams(
				sizes.get(sizePos),
				qualities.get(qualityPos),
				orderQuantitySpinner.getSelectedItemPosition(),
				price
			);

			orders.add(order);
			orderAdapterPrint.onAddNotify(orders);

			onItemClickImage(0);
			onItemClickChanger();

			tutorial = false;
			sharedPreferencesService.incrementTooltipShowCount();
			onResume();
		});

		dataLoaded = true;
		onResume();
	}

	private void setCarouselVisibility(int visibility) {
		carouselRecyclerView.setVisibility(visibility);
		radioGroup.setVisibility(visibility);
		carouselTextView.setVisibility(visibility);
	}

	private float ordersGetResult() {
		float result = 0;
		for (Order order : orders)
			result = result + order.getPrice();
		return result;
	}

	public void onItemClickChanger() {
		onResume();
	}

	public void onItemClickDelete(int position) {
		orders.remove(position);
		orderAdapterPrint.onAddNotify(orders);
		onResume();
	}

	public void onItemClickImage(int position) {
		this.position = position;
		Intent i = new Intent();
		i.setType("image/*");
		i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
		i.setAction(Intent.ACTION_GET_CONTENT);
		startActivityForResult(Intent.createChooser(i, "Выберите файл"), RequestCodes.PICTURE_REQUEST_CODE.getRequestCode());
		onResume();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == RequestCodes.LOGIN_CODE.getRequestCode() && resultCode == RESULT_OK) {
			listenDatabase();
		}

		if (requestCode == RequestCodes.PICTURE_REQUEST_CODE.getRequestCode() && resultCode == RESULT_OK && null != data) {
			if (data.getClipData() == null) addNewOrder(data.getData());
			else for (int i = 0; i < data.getClipData().getItemCount(); i++)
				addNewOrder(data.getClipData().getItemAt(i).getUri());

			orderAdapterPrint.onAddNotify(orders);
			onResume();
		}
		if (requestCode == RequestCodes.CAROUSEL_SHOW.getRequestCode() && resultCode == RESULT_OK) {
			Intent i = new Intent();
			i.setType("image/*");
			i.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
			i.setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(i, "Выберите файл"), RequestCodes.PICTURE_REQUEST_CODE.getRequestCode());
			onResume();
		}
		if (requestCode == RequestCodes.ACTIVITY_PROGRESS.getRequestCode())
			if (resultCode == RESULT_OK) {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				builder.setTitle("Загрузка завершена.");
				builder.setMessage("Загружено: " + (orders.size() - 1) + " файлов.");
				builder.setPositiveButton("Ок", (dialog, which) -> {
					orders.clear();
					addNewOrder();

					sharedPreferencesService.incrementOrderCount();
					long date = Calendar.getInstance().getTime().getTime();
					if (sharedPreferencesService.getOrderCount() >= 1 && sharedPreferencesService.getLastRateShow() < date - 7776000000L /* 3 mon */) {
						showRateDialog();
						sharedPreferencesService.setLastRateShow(date);
					} else if (sharedPreferencesService.getOrderCount() >= 2 && sharedPreferencesService.getLastShareShow() < date - 15638400000L /* 6 mon */) {
						showShareDialog();
						sharedPreferencesService.setLastShareShow(date);
					} else onResume();
				});
				builder.show();
			}
		if (requestCode == RequestCodes.SHARE_ACTIVITY.getRequestCode() && isShowDialog) {
			showShareDialog();
		}
		if (requestCode == RequestCodes.MARKET_ACTIVITY.getRequestCode() && isShowDialog) {
			showDialogLayout();
		}
	}

	private void addNewOrder(Uri selectedImage) {
		Order order = new Order(selectedImage, orders.get(position));
		order.setPrice(priceMapSingleton.getPriceMap().getPriceByValues(order.getQuality(), order.getSize()));
		orders.add(position == 0 ? 1 : position, order);
	}

	private void expandPrintLayout() {
		isExpandPrintLayout = !isExpandPrintLayout;

		blackoutLayout.setElevation(dp(10));

		float startAlphaAnimator = isExpandPrintLayout ? 0f : 1f;
		float endAlphaAnimator = isExpandPrintLayout ? 1f : 0f;

		final ValueAnimator alphaAnimator = ValueAnimator.ofFloat(startAlphaAnimator, endAlphaAnimator);
		alphaAnimator.setDuration(200);
		alphaAnimator.addUpdateListener(animation -> {
			blackoutLayout.setAlpha((Float) animation.getAnimatedValue() / 3f);
			cancelPrintButton.setAlpha((Float) animation.getAnimatedValue());
			textViewResult.setAlpha(1f - (Float) animation.getAnimatedValue());
			deliveryTextView.setAlpha((Float) animation.getAnimatedValue());
			lastResultTextView.setAlpha((Float) animation.getAnimatedValue());
			if (animation.getAnimatedValue().equals(0f)) {
				int visibility = isExpandPrintLayout ? View.VISIBLE : View.INVISIBLE;
				blackoutLayout.setVisibility(visibility);
				cancelPrintButton.setVisibility(visibility);
			}
		});
		alphaAnimator.start();

		ConstraintLayout.LayoutParams lastResultTextViewLayoutParams = ((ConstraintLayout.LayoutParams) lastResultTextView.getLayoutParams());
		ConstraintLayout.LayoutParams deliveryTextViewLayoutParams = ((ConstraintLayout.LayoutParams) deliveryTextView.getLayoutParams());
		printButtonLayoutParams = (ConstraintLayout.LayoutParams) printButton.getLayoutParams();

		int closedValue = printButtonLayoutParams.bottomMargin;
		int expandedValue = lastResultTextViewLayoutParams.topMargin +
			lastResultTextViewLayoutParams.getConstraintWidget().getHeight() +
			lastResultTextViewLayoutParams.bottomMargin +
			deliveryTextViewLayoutParams.getConstraintWidget().getHeight() +
			deliveryTextViewLayoutParams.bottomMargin +
			dp(10) +
			closedValue;

		int startValueAnimator = isExpandPrintLayout ? closedValue : expandedValue;
		int endValueAnimator = isExpandPrintLayout ? expandedValue : closedValue;

		final ValueAnimator valueAnimator = ValueAnimator.ofInt(startValueAnimator, endValueAnimator);
		valueAnimator.setDuration(200);
		valueAnimator.addUpdateListener(animation -> {
			printButtonLayoutParams.topMargin = (Integer) valueAnimator.getAnimatedValue();
			printButton.setLayoutParams(printButtonLayoutParams);
		});
		valueAnimator.start();
	}

	public void showDialogLayout() {
		isShowDialog = !isShowDialog;

		blackoutLayout.setElevation(dp(30));

		float startAlphaAnimator = isShowDialog ? 0f : 1f;
		float endAlphaAnimator = isShowDialog ? 1f : 0f;

		final ValueAnimator alphaAnimator = ValueAnimator.ofFloat(startAlphaAnimator, endAlphaAnimator);
		alphaAnimator.setDuration(200);
		alphaAnimator.addUpdateListener(animation -> {
			blackoutLayout.setAlpha((Float) animation.getAnimatedValue() / 2.5f);
			dialogLayout.setAlpha((Float) animation.getAnimatedValue());
			if (animation.getAnimatedValue().equals(0f)) {
				int visibility = isShowDialog ? View.VISIBLE : View.INVISIBLE;
				blackoutLayout.setVisibility(visibility);
				dialogLayout.setVisibility(visibility);
			}
		});
		alphaAnimator.start();
	}

	public void showShareDialog() {
		dialog.setDialogTitle(R.string.share_dialog_title);

		dialog.setLeftButton(R.string.decline_button, v -> {
			showDialogLayout();
			onResume();
		});

		dialog.setRightButton(R.string.confirm_button, v -> {
			startShareActivity();

			onResume();
		});

		showDialogLayout();
	}

	private void startShareActivity() {
		Intent sharingIntent = new Intent(Intent.ACTION_SEND);
		sharingIntent.setType("text/plain");
		String shareBody = "Я печатаю фото через приложение \"Улыбочку!\". И тебе рекомендую!\n\nСкачать: https://dpi.by/foto";
		sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject Here");
		sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
		startActivityForResult(Intent.createChooser(sharingIntent, "Поделиться через"), RequestCodes.ACTIVITY_PROGRESS.getRequestCode());
	}

	public void showRateDialog() {
		dialog.setDialogTitle(R.string.rate_dialog_title);

		dialog.setLeftButton(R.string.decline_button, v -> {
			showDialogLayout();
			onResume();
		});

		dialog.setRightButton(R.string.confirm_button, v -> {
			startRateActivity();

			onResume();
		});

		showDialogLayout();
	}

	private void startRateActivity() {
		final String appPackageName = getPackageName();
		try {
			startActivityForResult(
				new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)),
				RequestCodes.MARKET_ACTIVITY.getRequestCode());
		} catch (android.content.ActivityNotFoundException e) {
			startActivityForResult(
				new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)),
				RequestCodes.MARKET_ACTIVITY.getRequestCode());
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		carouselTimer.cancel();
		database.child("Prices").removeEventListener(postListener);
		pricesMapDisposable.dispose();
	}
}
