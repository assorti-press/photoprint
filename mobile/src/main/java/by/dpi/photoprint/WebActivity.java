package by.dpi.photoprint;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class WebActivity extends AppCompatActivity {

	private android.webkit.WebView webView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);
		webView = findViewById(R.id.webWiew);
		if(getIntent().getStringExtra("content").contains(".html")){
			webView.getSettings().setDefaultTextEncodingName("UTF-8");
			webView
				.loadUrl("file:///android_res/raw/" + getIntent().getStringExtra("content"));}
		else
		 webView.loadData(getIntent().getStringExtra("content"), "text/html; charset=utf-8", "UTF-8");
	}
}
