package by.dpi.photoprint;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface JSONPlaceHolderApi {
    @Headers("Authorization: OAuth" + BuildConfig.OAUTH_YA)
    @PUT("resources")
    Call<ResponseBody> createPath(@Query("path") String path);

    @Headers("Authorization: OAuth" + BuildConfig.OAUTH_YA)
    @POST("resources/upload")
    Call<ResponseBody> uploadFile(@Query("path") String path, @Query("url") String link);
}
