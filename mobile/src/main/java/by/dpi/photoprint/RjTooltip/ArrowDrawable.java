package by.dpi.photoprint.RjTooltip;

import android.graphics.*;
import android.graphics.drawable.ColorDrawable;
import androidx.annotation.ColorInt;

public class ArrowDrawable extends ColorDrawable {

    private final Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private final int backgroundColor;
    private Path path;

    public ArrowDrawable(@ColorInt int foregroundColor) {
        this.backgroundColor = Color.TRANSPARENT;
        this.paint.setColor(foregroundColor);
    }

    private synchronized void updatePath(Rect bounds) {
        path = new Path();
				path.moveTo(0, 0);
				path.lineTo(bounds.width() / 2, bounds.height());
				path.lineTo(bounds.width(), 0);
				path.lineTo(0, 0);
        path.close();
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawColor(backgroundColor);
        if (path == null)
            updatePath(getBounds());
        canvas.drawPath(path, paint);
    }
}
