package by.dpi.photoprint.RjTooltip;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.*;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.annotation.*;
import by.dpi.photoprint.R;

public class Tooltip {

	private final Context context;
	private final CharSequence text;
	private final View anchorView;
	private PopupWindow popupWindow;
	private View contentLayout;
	private View contentView;
	@IdRes
	private int textViewId;
	private ViewGroup rootView;
	private ImageView arrowView;
	private AnimatorSet animator;
	private boolean dismissed = false;

	private final ViewTreeObserver.OnGlobalLayoutListener showLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			final PopupWindow popup = popupWindow;
			if (popup == null || dismissed) return;

			popup.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

			contentLayout.setVisibility(View.VISIBLE);
		}
	};

	private final ViewTreeObserver.OnGlobalLayoutListener autoDismissLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			final PopupWindow popup = popupWindow;
			if (popup == null || dismissed) return;

			if (!rootView.isShown()) dismiss();
		}
	};

	private final ViewTreeObserver.OnGlobalLayoutListener arrowLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			final PopupWindow popup = popupWindow;
			if (popup == null || dismissed) return;

			popup.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(this);

			popup.getContentView().getViewTreeObserver().addOnGlobalLayoutListener(animationLayoutListener);
			popup.getContentView().getViewTreeObserver().addOnGlobalLayoutListener(showLayoutListener);

			RectF achorRect = rectOnScreenCalculate(anchorView);
			RectF contentViewRect = rectOnScreenCalculate(contentLayout);
			float x, y;

			x = contentLayout.getPaddingLeft() + getPx(2);
			float centerX = (contentViewRect.width() / 2f) - (arrowView.getWidth() / 2f);
			float newX = centerX - (contentViewRect.centerX() - achorRect.centerX());
			if (newX > x) {
				if (newX + arrowView.getWidth() + x > contentViewRect.width()) {
					x = contentViewRect.width() - arrowView.getWidth() - x;
				} else {
					x = newX;
				}
			}
			y = arrowView.getTop();
			y = y - 1;

			arrowView.setX(x);
			arrowView.setY(y);

			popup.getContentView().requestLayout();
		}
	};
	private Drawable arrowDrawable;
	private float margin;
	private float padding;
	private float animationPadding;
	private long animationDuration;
	private final ViewTreeObserver.OnGlobalLayoutListener animationLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			final PopupWindow popup = popupWindow;
			if (popup == null || dismissed) return;
			popup.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
			startAnimation();
			popup.getContentView().requestLayout();
		}
	};
	private float arrowHeight;
	private float arrowWidth;
	private final int width = ViewGroup.LayoutParams.WRAP_CONTENT;
	private final int height = ViewGroup.LayoutParams.WRAP_CONTENT;
	private final ViewTreeObserver.OnGlobalLayoutListener locationLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
		@Override
		public void onGlobalLayout() {
			final PopupWindow popup = popupWindow;
			if (popup == null || dismissed) return;


			popup.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
			popup.getContentView().getViewTreeObserver().addOnGlobalLayoutListener(arrowLayoutListener);
			PointF location = popupLocationConfigure();
			popup.setClippingEnabled(true);
			popup.update((int) location.x, (int) location.y, popup.getWidth(), popup.getHeight());
			popup.getContentView().requestLayout();
		}
	};
	public static float getPx(float dp) {
		return dp * Resources.getSystem().getDisplayMetrics().density;
	}

	private Tooltip(Context context, View anchorView, CharSequence text) {
		this.context = context;
		this.anchorView = anchorView;
		this.text = text;
	}

	public static Tooltip make(Context context, View anchorView, CharSequence text) {
		return new Tooltip(context, anchorView, text);
	}

	public static Tooltip make(Context context, View anchorView, @StringRes int textRes) {
		return new Tooltip(context, anchorView, context.getString(textRes));
	}

	private void init() {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		contentView = inflater.inflate(R.layout.tooltip_view, null, false);
		textViewId = R.id.tooltip_text;

		margin = context.getResources().getDimension(R.dimen.tooltip_margin);
		padding = context.getResources().getDimension(R.dimen.tooltip_padding);
		animationPadding = context.getResources().getDimension(R.dimen.tooltip_animation_padding);
		animationDuration = context.getResources().getInteger(R.integer.tooltip_animation_duration);

		arrowDrawable = new ArrowDrawable(context.getResources().getColor(R.color.tooltip_arrow));
		arrowWidth = context.getResources().getDimension(R.dimen.tooltip_arrow_width);
		arrowHeight = context.getResources().getDimension(R.dimen.tooltip_arrow_height);

		rootView = (ViewGroup) anchorView.getRootView();

		popupWindowConfigure();
		contentViewConfigure();
	}

	@SuppressLint("ClickableViewAccessibility")
	private void popupWindowConfigure() {
		popupWindow = new PopupWindow(context, null, android.R.attr.popupWindowStyle);
		popupWindow.setOnDismissListener(dismissListener());
		popupWindow.setWidth(width);
		popupWindow.setHeight(height);
		popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		popupWindow.setOutsideTouchable(true);
		popupWindow.setTouchable(true);
		popupWindow.setTouchInterceptor((v, event) -> {
			dismiss();
			return true;
		});
		popupWindow.setClippingEnabled(false);
		popupWindow.setFocusable(false);
	}

	private void contentViewConfigure() {
		if (contentView instanceof TextView) {
			TextView textView = (TextView) contentView;
			textView.setText(text);
		} else {
			TextView textView = contentView.findViewById(textViewId);
			if (textView != null)
				textView.setText(text);
		}

		contentView.setPadding((int) padding, (int) padding, (int) padding, (int) padding);

		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
		linearLayout.setOrientation(LinearLayout.VERTICAL);
		int layoutPadding = (int) animationPadding;
		linearLayout.setPadding(layoutPadding, layoutPadding, layoutPadding, layoutPadding);


		arrowView = new ImageView(context);
		arrowView.setImageDrawable(arrowDrawable);
		arrowView.setElevation(30f);
		LinearLayout.LayoutParams arrowLayoutParams;

		arrowLayoutParams = new LinearLayout.LayoutParams((int) arrowWidth, (int) arrowHeight, 0);

		arrowLayoutParams.gravity = Gravity.CENTER;
		arrowView.setLayoutParams(arrowLayoutParams);
		linearLayout.addView(contentView);
		linearLayout.addView(arrowView);
		LinearLayout.LayoutParams contentViewParams = new LinearLayout.LayoutParams(width, height, 0);
		contentViewParams.gravity = Gravity.CENTER;
		contentView.setLayoutParams(contentViewParams);

		contentLayout = linearLayout;
		contentLayout.setVisibility(View.INVISIBLE);
		popupWindow.setContentView(contentLayout);
	}

	public void show() {
		init();

		contentLayout.getViewTreeObserver().addOnGlobalLayoutListener(locationLayoutListener);
		contentLayout.getViewTreeObserver().addOnGlobalLayoutListener(autoDismissLayoutListener);

		rootView.post(() -> {
			if (rootView.isShown())
				popupWindow.showAtLocation(rootView, Gravity.NO_GRAVITY, rootView.getWidth(), rootView.getHeight());
			else
				Log.e("Tooltip", "Tooltip cannot be shown, root view is invalid or has been closed.");
		});
	}

	private PointF popupLocationConfigure() {
		PointF location = new PointF();

		final RectF anchorRect = rectInWindowCalculate(anchorView);
		final PointF anchorCenter = new PointF(anchorRect.centerX(), anchorRect.centerY());

		location.x = anchorCenter.x - popupWindow.getContentView().getWidth() / 2f;
		location.y = anchorRect.top - popupWindow.getContentView().getHeight() - margin;

		return location;
	}

	public static RectF rectOnScreenCalculate(View view) {
		int[] location = new int[2];
		view.getLocationOnScreen(location);
		return new RectF(location[0], location[1], location[0] + view.getMeasuredWidth(), location[1] + view.getMeasuredHeight());
	}

	public static RectF rectInWindowCalculate(View view) {
		int[] location = new int[2];
		view.getLocationInWindow(location);
		return new RectF(location[0], location[1], location[0] + view.getMeasuredWidth(), location[1] + view.getMeasuredHeight());
	}

	public void dismiss() {
		if (dismissed)
			return;

		dismissed = true;
		if (popupWindow != null) {
			popupWindow.dismiss();
		}
	}

	public boolean isShowing() {
		return popupWindow != null && popupWindow.isShowing();
	}

	public PopupWindow.OnDismissListener dismissListener() {
		return () -> {
			dismissed = true;

			if (animator != null) {
				animator.removeAllListeners();
				animator.end();
				animator.cancel();
				animator = null;
			}

			rootView = null;

			popupWindow.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(locationLayoutListener);
			popupWindow.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(arrowLayoutListener);
			popupWindow.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(showLayoutListener);
			popupWindow.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(animationLayoutListener);
			popupWindow.getContentView().getViewTreeObserver().removeOnGlobalLayoutListener(autoDismissLayoutListener);

			popupWindow = null;
		};
	}

	private void startAnimation() {
		final String property = "translationY";

		final ObjectAnimator anim1 = ObjectAnimator.ofFloat(contentLayout, property, -animationPadding,
						animationPadding);
		anim1.setDuration(animationDuration);
		anim1.setInterpolator(new AccelerateDecelerateInterpolator());

		final ObjectAnimator anim2 = ObjectAnimator.ofFloat(contentLayout, property, animationPadding, -animationPadding);
		anim2.setDuration(animationDuration);
		anim2.setInterpolator(new AccelerateDecelerateInterpolator());

		animator = new AnimatorSet();
		animator.playSequentially(anim1, anim2);
		animator.addListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				if (!dismissed && isShowing()) {
					animation.start();
				}
			}
		});
		animator.start();
	}
}
