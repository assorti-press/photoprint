package by.dpi.photoprint;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import by.dpi.photoprint.model.User;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginActivity extends AppCompatActivity {

	private SharedPreferencesService sharedPreferencesService;

	private FirebaseAuth firebaseAuth;

	private FirebaseDatabase firebaseDatabase;
	private DatabaseReference databaseReference;

	private TextView textViewTel;

	private Button buttonLogin;
	private Button button0;
	private Button button1;
	private Button button2;
	private Button button3;
	private Button button4;
	private Button button5;
	private Button button6;
	private Button button7;
	private Button button8;
	private Button button9;
	private Button buttonDel;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		sharedPreferencesService = new SharedPreferencesService(getApplicationContext());

		firebaseDatabase = FirebaseDatabase.getInstance();

		firebaseAuth = FirebaseAuth.getInstance();

		textViewTel = findViewById(R.id.textViewTelNumber);

		buttonLogin = findViewById(R.id.loginButton);
		button0 = findViewById(R.id.zeroButton);
		button1 = findViewById(R.id.oneButton);
		button2 = findViewById(R.id.twoButton);
		button3 = findViewById(R.id.threeButton);
		button4 = findViewById(R.id.fourButton);
		button5 = findViewById(R.id.fiveButton);
		button6 = findViewById(R.id.sixButton);
		button7 = findViewById(R.id.sevenButton);
		button8 = findViewById(R.id.eightButton);
		button9 = findViewById(R.id.nineButton);
		buttonDel = findViewById(R.id.deleteButton);

		onChangeVerify();
		setListeners();
	}

	private void setListeners() {
		buttonLogin.setOnClickListener(this::anonymousAuth);

		button0.setOnClickListener(addNumber("0"));
		button1.setOnClickListener(addNumber("1"));
		button2.setOnClickListener(addNumber("2"));
		button3.setOnClickListener(addNumber("3"));
		button4.setOnClickListener(addNumber("4"));
		button5.setOnClickListener(addNumber("5"));
		button6.setOnClickListener(addNumber("6"));
		button7.setOnClickListener(addNumber("7"));
		button8.setOnClickListener(addNumber("8"));
		button9.setOnClickListener(addNumber("9"));

		buttonDel.setOnClickListener(v -> {
			String str = textViewTel.getText().toString();
			if (str.length() > 4) {
				textViewTel.setText(str.substring(0, str.length() - 1));
				onChangeVerify();
			}
		});
	}

	void anonymousAuth(View v) {
		if(textViewTel.length() < 13 && getIntent().getBooleanExtra("change_number", false)) finish();
		else if(textViewTel.length() == 13){
			firebaseAuth.signInAnonymously().addOnCompleteListener(task -> {
				if (task.isSuccessful()) {
					FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();

					User user = new User(
						textViewTel.getText().toString(),
						sharedPreferencesService.getPhoneNumber()
					);

					sharedPreferencesService.setPhoneNumber(user.getPhoneNumber());

					assert firebaseUser != null;
					databaseReference = firebaseDatabase.getReference(
						BuildConfig.DATABASE_COL_USERS + firebaseUser.getUid() + "/");
					databaseReference.setValue(user);

					finish();
				} else {
					Log.w("AnonymousAuth", "signInAnonymously:failure", task.getException());

					Snackbar snackbar = Snackbar.make(v, "Ошибка входа", Snackbar.LENGTH_LONG);
					snackbar.setAction("Повторить", this::anonymousAuth);
					snackbar.show();
				}
			});
		}
	}

	@SuppressLint("SetTextI18n")
	private View.OnClickListener addNumber(String number) {
		return v -> {
			if (textViewTel.length() < 13)
				textViewTel.setText(textViewTel.getText() + number);
				onChangeVerify();
		};
	}

	private void onChangeVerify(){
		boolean changeNumber = textViewTel.length() <= 4 && getIntent().getBooleanExtra("change_number", false);

		if (changeNumber) {
			buttonLogin.setText(R.string.action_sign_in_back);
		}	else buttonLogin.setText(R.string.action_sign_in);

		if (textViewTel.length() == 13 || changeNumber) {
			buttonLogin.setBackgroundResource(R.drawable.shape);
			buttonLogin.setEnabled(true);
		} else {
			buttonLogin.setBackgroundResource(R.drawable.shape_off);
			buttonLogin.setEnabled(false);
		}
	}

	@Override
	public void onBackPressed() {
		if(getIntent().getBooleanExtra("change_number", false)) finish();
	}
}
