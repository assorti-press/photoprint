package by.dpi.photoprint;

import android.widget.ProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.lifecycle.Observer;
import by.dpi.photoprint.model.Result;

public class ProgressActivity extends AppCompatActivity {

    private ProgressBar processBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress);

        processBar = findViewById(R.id.progressBar);
        processBar.setMax(MainViewModel.getInstance().getOrders().size());
        MainViewModel.getInstance().uploadList().observe(this, new Observer<Result>() {
            @Override
            public void onChanged(Result result) {
                processBar.setProgress(result.progress + 1);
                if (result.onComplete != -100) {
                    setResult(result.onComplete);
                    result.onComplete = -100;
                    finish();
                }
            }
        });

    }
}