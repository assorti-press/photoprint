package by.dpi.photoprint.model;

import java.util.List;

public class PriceWrap {
   List<PriceList> array;

   public PriceWrap() {
   }

   public PriceWrap(List<PriceList> array) {
      this.array = array;
   }

   public List<PriceList> getPrices() {
      return array;
   }

   public void setPrices(List<PriceList> array) {
      this.array = array;
   }
}
