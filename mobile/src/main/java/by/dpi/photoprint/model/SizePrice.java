package by.dpi.photoprint.model;

import java.util.Objects;

public class SizePrice {
   Size size;
   float price;

   public SizePrice() {
   }

   public SizePrice(Size size, float price) {
      this.size = size;
      this.price = price;
   }

   public Size getSize() {
      return size;
   }

   public void setSize(Size size) {
      this.size = size;
   }

   public float getPrice() {
      return price;
   }

   public void setPrice(float price) {
      this.price = price;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SizePrice sizePrice = (SizePrice) o;
      return size.equals(sizePrice.size);
   }

   @Override
   public int hashCode() {
      return Objects.hash(size);
   }
}
