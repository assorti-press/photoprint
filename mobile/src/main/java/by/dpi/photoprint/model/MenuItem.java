package by.dpi.photoprint.model;

public class MenuItem {
	private String name;
	private String name_ru;
	private String html;
	private int orderPrior;

	public MenuItem() {
	}

	public MenuItem(String name, String name_ru, String html, int orderPrior) {
		this.name = name;
		this.name_ru = name_ru;
		this.html = html;
		this.orderPrior = orderPrior;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName_ru() {
		return name_ru;
	}

	public void setName_ru(String name_ru) {
		this.name_ru = name_ru;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public int getOrderPrior() {
		return orderPrior;
	}

	public void setOrderPrior(int orderPrior) {
		this.orderPrior = orderPrior;
	}
}
