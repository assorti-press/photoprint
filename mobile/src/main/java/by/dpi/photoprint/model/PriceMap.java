package by.dpi.photoprint.model;

import androidx.annotation.NonNull;

import java.util.*;

public class PriceMap {
	private Map<String, Map<String, Float>> priceMap;
	private List<String> qualities;
	private List<String> sizes;

	public PriceMap(Map<String, Map<String, Float>> priceMap, List<String> qualities, List<String> sizes) {
		this.priceMap = priceMap;
		this.qualities = qualities;
		this.sizes = sizes;
	}

	public Map<String, Map<String, Float>> getPriceMap() {
		return priceMap;
	}

	public void setPriceMap(Map<String, Map<String, Float>> priceMap) {
		this.priceMap = priceMap;
	}

	public List<String> getQualities() {
		return qualities;
	}

	public void setQualities(List<String> qualities) {
		this.qualities = qualities;
	}

	public List<String> getSizes() {
		return sizes;
	}

	public void setSizes(List<String> sizes) {
		this.sizes = sizes;
	}

	public List<String> getQualitiesBySize(String size) {

		List<String> returnList = new ArrayList<>();
		for(String quality : qualities) {
			if(Objects.requireNonNull(priceMap.get(quality)).containsKey(size))
				returnList.add(quality);
		}

		return returnList;
	}

	public float getPriceBySelection(int qualityPos, int sizePos) {
		String quality = qualities.get(qualityPos);
		String size = sizes.get(sizePos);

		return getPriceByValues(quality, size);
	}

	public float getPriceByValues(String quality, String size) {
		try {
			Map<String, Float> priceList = priceMap.get(quality);
			assert priceList != null;
			Float price = priceList.get(size);
			assert price != null;
			return price;
		} catch (Exception e) {
			e.printStackTrace();
			return .0f;
		}
	}
}
