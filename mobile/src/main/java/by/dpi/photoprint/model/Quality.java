package by.dpi.photoprint.model;

public class Quality {
   int order;
   String name;
   String text;

   public Quality() {
   }

   public Quality(int order, String name, String text) {
      this.order = order;
      this.name = name;
      this.text = text;
   }

   public int getOrder() {
      return order;
   }

   public void setOrder(int order) {
      this.order = order;
   }

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getText() {
      return text;
   }

   public void setText(String text) {
      this.text = text;
   }
}
