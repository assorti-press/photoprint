package by.dpi.photoprint.model;

import java.util.*;

public class PriceMapSingleton {
	private final static PriceMapSingleton INSTANCE = new PriceMapSingleton();

	private PriceMap priceMap;

	private PriceMapSingleton() {
	}

	public static PriceMapSingleton getInstance() {
		return INSTANCE;
	}

	public PriceMap firebaseConvert(List<PriceList> priceList) {
		Map<String, Map<String, Float>> newPriceMap = new HashMap<>();
		ArrayList<String> newQualities = new ArrayList<>();
		Set<String> newSizes = new TreeSet<>();

		for(PriceList list : priceList) {
			String quality = list.getQuality().getName();
			newQualities.add(quality);
			newPriceMap.put(quality, new HashMap<>());

			for(SizePrice sizePrice : list.getSizePrices()) {
				String size = sizePrice.getSize().getWidth() + "x" + sizePrice.getSize().getHeight();
				newSizes.add(size);
				Objects.requireNonNull(newPriceMap.get(quality)).put(size, sizePrice.getPrice());
			}
		}

		List<String> reversedQualities = new ArrayList<>(newQualities);
		List<String> sortedSizes = new ArrayList<>(newSizes);
		Collections.reverse(reversedQualities);
		Collections.sort(sortedSizes);

		return new PriceMap(newPriceMap, reversedQualities, sortedSizes);
	}

	public PriceMap getPriceMap() {
		return priceMap;
	}

	public void setPriceMap(PriceMap priceMap) {
		this.priceMap = priceMap;
	}
}
