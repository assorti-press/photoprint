package by.dpi.photoprint.model;

import android.net.Uri;
import androidx.annotation.NonNull;
import by.dpi.photoprint.OrderAdapterPrint;

import java.io.File;

public class Order {

	private String size;
	private String quality;
	private int quantity;
	private float price;

	private int num;

	private final Uri uri;

	public Order() {
		init();
		this.uri = OrderAdapterPrint.defaultUri;
	}

	public Order(Uri uri) {
		init();
		this.uri = uri;
	}

	public Order(Uri uri, Order order) {
		this.size = order.getSize();
		this.quality = order.getQuality();
		this.quantity = order.getQuantity();
		this.price = order.getPrice();
		this.uri = uri;
	}

	public Order(String size, String quality, int quantity, float price) {
		this.uri = OrderAdapterPrint.defaultUri;
		this.size = size;
		this.quality = quality;
		this.quantity = quantity;
		this.price = price;
	}

	private void init() {
		price = 0.0f;
		size = "";
		quality = "";
		quantity = 1;
		num = 1;
	}

	public float getPrice() {
		float ret;
		ret = price * quantity;
		if (uri.equals(OrderAdapterPrint.defaultUri)) ret = 0;
		return ret;
	}

	public String getSinglePrice() {
		return price + "P";
	}

	@Override
	@NonNull
	public String toString() {
		return "Order{" +
			"size='" + size + '\'' +
			", quality='" + quality + '\'' +
			", quantity=" + quantity +
			", price=" + price +
			", num=" + num +
			", uri=" + uri +
			'}';
	}

	public String getFileDestination() {
		File file = new File(uri.getPath());
		String filename = file.getName();
		filename = filename.replace(":", "-");
		return quality + " - " +
			size + " x " +
			quantity + "шт" +
			" file " +
			filename;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public Uri getUri() {
		return uri;
	}

	public void setParams(String size, String quality, int quantity, float price) {
		this.size = size;
		this.quality = quality;
		this.quantity = quantity;
		this.price = price;
	}
}
