package by.dpi.photoprint;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

class DialogLayout {

    private final TextView dialogTitle;

    private final Button leftButton;
    private final Button rightButton;

    DialogLayout(MainActivity mainActivity) {
        dialogTitle = mainActivity.findViewById(R.id.dialogTitle);

        leftButton = mainActivity.findViewById(R.id.leftButton);
        rightButton = mainActivity.findViewById(R.id.rightButton);
    }

    void setDialogTitle(int stringId) {
        dialogTitle.setText(stringId);
    }

    void setLeftButton(int stringId, View.OnClickListener l) {
        leftButton.setText(stringId);
        leftButton.setOnClickListener(l);
    }

    void setRightButton(int stringId, View.OnClickListener l) {
        rightButton.setText(stringId);
        rightButton.setOnClickListener(l);
    }
}
