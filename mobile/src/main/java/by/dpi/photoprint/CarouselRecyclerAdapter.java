package by.dpi.photoprint;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

class CarouselRecyclerAdapter extends RecyclerView.Adapter<CarouselRecyclerAdapter.CarouselViewHolder> {

    private final List<Integer> carouselImages = new ArrayList<>();

    private final LayoutInflater carouselInflater;

    private final MainActivity mainActivity;

    public CarouselRecyclerAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        carouselInflater = LayoutInflater.from(mainActivity);
        carouselImages.add(R.drawable.sc01);
        carouselImages.add(R.drawable.sc02);
        carouselImages.add(R.drawable.sc03);
    }

    @NonNull
    @Override
    public CarouselViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = carouselInflater.inflate(R.layout.item_carousel, parent, false);
        return new CarouselViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull final CarouselViewHolder holder, final int positionAdapter) {
        Picasso
                .get()
                .load(carouselImages.get(positionAdapter))
                .into(holder.carouselImageView);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull CarouselViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        mainActivity.onCarouselItemClick(holder.getLayoutPosition());
    }

    @Override
    public void onViewRecycled(@NonNull CarouselViewHolder holder) {
        super.onViewRecycled(holder);
    }

    public int getItemCount() {
        return carouselImages.size();
    }

    public static class CarouselViewHolder extends RecyclerView.ViewHolder {
        private final ImageView carouselImageView;

        public CarouselViewHolder(View view) {
            super(view);
            carouselImageView = view.findViewById(R.id.carouselImageView);
        }
    }
}
