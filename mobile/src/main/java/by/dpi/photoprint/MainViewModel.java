package by.dpi.photoprint;

import android.app.Activity;
import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import by.dpi.photoprint.model.Order;
import io.reactivex.rxjava3.observers.DisposableObserver;
import by.dpi.photoprint.model.Result;

import java.util.List;

public class MainViewModel extends AndroidViewModel {

    private static MainViewModel instance;

    private Engine engine;
    private List<Order> orders;
    private MutableLiveData<Result> resultMut = new MutableLiveData<>();
    private Result result;

    public MainViewModel(@NonNull Application application) {
        super(application);
        instance = this;
        this.engine = new Engine(getApplication().getApplicationContext());
        this.result = new Result();
    }

    void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return orders;
    }

    LiveData<Result> uploadList() {
        engine.uploadList(orders).subscribe(new DisposableObserver<Integer>() {
            @Override
            public void onNext(@io.reactivex.rxjava3.annotations.NonNull Integer progress) {

                result.progress = progress;
                resultMut.postValue(result);
            }

            @Override
            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                result.onComplete = Activity.RESULT_CANCELED;
                resultMut.postValue(result);
            }

            @Override
            public void onComplete() {
                result.onComplete = Activity.RESULT_OK;
                resultMut.postValue(result);
            }
        });
        return resultMut;
    }

    public static MainViewModel getInstance() {
        return instance;
    }

}
