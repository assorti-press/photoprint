package by.dpi.photoprint;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import androidx.annotation.NonNull;
import by.dpi.photoprint.model.Order;
import by.dpi.photoprint.model.OrderNotification;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.*;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import java.io.File;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.CountDownLatch;

public class Engine {

    private final NetworkService networkService;
    private JSONPlaceHolderApi jsonPlaceHolderApi;
    private Context contex;
    private File file;
    private String link;
    private StorageReference orderRef;
    private StorageReference mStorageRef;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    private FirebaseAuth mAuth;
    private Integer i = 0;
    private Order orderThis;
    private CountDownLatch count;
    private SharedPreferencesService preferences;
    private String tel;


    public Engine(Context contex) {
        networkService = NetworkService.getInstance();
        jsonPlaceHolderApi = networkService.getJSONApi();
        this.contex = contex;
        mStorageRef = FirebaseStorage.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();

        database = FirebaseDatabase.getInstance();
        databaseReference  = database.getReference(BuildConfig.DATABASE_COL_ORDERS + mAuth.getUid() + "/");

        preferences = new SharedPreferencesService(contex);
    }


public Observable<Integer> uploadList(final List<Order> orders) {
            tel = preferences.getPhoneNumber();
            return Observable.create(new ObservableOnSubscribe<Integer>() {
            @Override
            public void subscribe(@io.reactivex.rxjava3.annotations.NonNull final ObservableEmitter<Integer> emitter) throws Throwable {
                i = 0;
                emitter.onNext(i);
                Order order ;
                for (; i < orders.size() ; ) {
                    order = orders.get(i);
                    if (!order.getUri().equals(OrderAdapterPrint.defaultUri)) {
                        count = new CountDownLatch(1);
                        orderThis = order;
                        uploadFileToStorage(order.getUri(), order.getFileDestination()).subscribe(new SingleObserver<Boolean>() {
                            @Override
                            public void onSubscribe(@io.reactivex.rxjava3.annotations.NonNull Disposable d) {

                            }

                            @Override
                            public void onSuccess(@io.reactivex.rxjava3.annotations.NonNull Boolean result) {
                                count.countDown();
                            }

                            @Override
                            public void onError(@io.reactivex.rxjava3.annotations.NonNull Throwable e) {
                                Log.d("DEBUG", "error " + e);
                                emitter.onError(new Throwable(e));
                                count.countDown();
                            }
                        });

                        count.await();
                        emitter.onNext(i++);
                    }
                    else i ++;
                }
            emitter.onComplete();
            }
        }).subscribeOn(Schedulers.io())
          .observeOn(AndroidSchedulers.mainThread());
}

    private Single<Boolean> uploadFileToStorage(final Uri uri, final String fieDestination) {
       return Single.create(new SingleOnSubscribe<Boolean>()  {
           @Override
           public void subscribe(@io.reactivex.rxjava3.annotations.NonNull final SingleEmitter<Boolean> emitter) throws Throwable {
               file = new File(uri.getPath());
               orderRef = mStorageRef.child(tel).child(fieDestination);
               orderRef.putFile(uri).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
                   @Override
                   public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                       orderRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                           @Override
                           public void onSuccess(Uri url) {
                               Log.d("DEBUG", "Patch = " + url);
                               link = url.toString();
                               jsonPlaceHolderApi.createPath(tel).enqueue(new Callback<ResponseBody>() {
                                   @Override
                                   public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                       jsonPlaceHolderApi.uploadFile("/" + tel + "/" + fieDestination + "_" + orderThis.getNum() + exeption(uri), link).enqueue(new Callback<ResponseBody>() {
                                           @Override
                                           public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                                               final Handler handler = new Handler();
                                               handler.postDelayed(new Runnable() {
                                                   @Override
                                                   public void run() {
                                                       emitter.onSuccess(true);
                                                   }
                                               }, 10000);

                                               if(response.code()==202) {
                                                   mAuth = FirebaseAuth.getInstance();
                                                   databaseReference  = database.getReference(BuildConfig.DATABASE_COL_ORDERS + mAuth.getUid() + "/");

                                                   OrderNotification order = new OrderNotification(tel, String.valueOf(Calendar.getInstance().getTimeInMillis()));
                                                   databaseReference.setValue(order);
                                               }
                                           }

                                           @Override
                                           public void onFailure(Call<ResponseBody> call, Throwable t) {
                                                emitter.onError(new Throwable("error order file"));
                                           }
                                       });
                                   }

                                   @Override
                                   public void onFailure(Call<ResponseBody> call, Throwable t) {

                                   }
                               });
                           }
                       });
                   }
               });

           }
       });

    }

    private String exeption(Uri uri) {
        String result = ".jpeg";
        String type = contex.getContentResolver().getType(uri);
        if (type != null)
            if(type.equals("")) ;
    return result;
    }

}
