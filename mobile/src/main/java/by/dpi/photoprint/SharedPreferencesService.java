package by.dpi.photoprint;

import android.content.Context;
import android.content.SharedPreferences;
import io.reactivex.rxjava3.core.Observable;

import java.util.Map;

public class SharedPreferencesService {
	final private SharedPreferences sharedPreferences;

	SharedPreferencesService(Context context) {
		sharedPreferences = context.getSharedPreferences("pref", Context.MODE_PRIVATE);
	}

	public String getPhoneNumber() {
		return sharedPreferences.getString("phone", "");
	}

	public void setPhoneNumber(String phoneNumber) {
		sharedPreferences.edit()
			.putString("phone", phoneNumber)
			.apply();
	}

	public boolean isPhoneNumberNull() {
		return getPhoneNumber().isEmpty();
	}

	public void incrementOrderCount() {
		String key = "order_count";
		sharedPreferences.edit()
			.putLong(key,
				sharedPreferences.getLong(key, 0L) + 1L)
			.apply();
	}

	public long getOrderCount() {
		return sharedPreferences.getLong("order_count", 0L);
	}

	public long getLastShareShow() {
		return sharedPreferences.getLong("last_share_show", 0L);
	}

	public void setLastShareShow(long date) {
		sharedPreferences.edit()
			.putLong("last_share_show", date)
			.apply();
	}

	public long getLastRateShow() {
		return sharedPreferences.getLong("last_rate_show", 0L);
	}

	public void setLastRateShow(long date) {
		sharedPreferences.edit()
			.putLong("last_rate_show", date)
			.apply();
	}

	public long getTooltipShowCount() {
		return sharedPreferences.getLong("tooltip_count", 0L);
	}

	public void incrementTooltipShowCount() {
		String key = "tooltip_count";
		sharedPreferences.edit()
			.putLong(key,
				sharedPreferences.getLong(key, 0L) + 1L)
			.apply();
	}
	public void clearTooltipShowCount() {
		String key = "tooltip_count";
		sharedPreferences.edit()
			.remove(key)
			.apply();
	}

	public long getLastAppLaunch() {
		return sharedPreferences.getLong("last_tooltip_show", 0L);
	}

	public void setLastAppLaunch(long date) {
		sharedPreferences.edit()
			.putLong("last_tooltip_show", date)
			.apply();
	}
}
