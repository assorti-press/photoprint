package by.dpi.photoprint;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import by.dpi.photoprint.model.Order;
import by.dpi.photoprint.model.PriceMapSingleton;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class OrderAdapterPrint extends RecyclerView.Adapter<OrderAdapterPrint.ViewHolderPrint> {

	public static Uri defaultUri;
	private final LayoutInflater layoutInflater;
	private final MainActivity mainActivity;
	private List<Order> orders;
	ViewHolderPrint viewHolderPrint;

	public OrderAdapterPrint(MainActivity mainActivity) {
		this.mainActivity = mainActivity;
		layoutInflater = LayoutInflater.from(mainActivity);
		defaultUri = Uri.parse("android.resource://" + mainActivity.getPackageName() + "/drawable/defaulturi");
	}

	@NonNull
	@Override
	public ViewHolderPrint onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View view = layoutInflater.inflate(R.layout.item_order, parent, false);
		return new ViewHolderPrint(view);
	}

	@Override
	public void onBindViewHolder(@NonNull final ViewHolderPrint holder, final int positionAdapter) {
		Uri uri = orders.get(positionAdapter).getUri();

		int visibleType;
		if (uri.equals(defaultUri))
			visibleType = View.GONE;
		else
			visibleType = View.VISIBLE;
		if(positionAdapter == 0)
			holder.orderBackground.setBackground(mainActivity.getDrawable(R.drawable.shape_order));
		else if(positionAdapter % 2 == 0)
			holder.orderBackground.setBackground(mainActivity.getDrawable(R.drawable.shape_order_first));
		else holder.orderBackground.setBackground(mainActivity.getDrawable(R.drawable.shape_order_second));

		if(viewHolderPrint==null && positionAdapter==0) {
			viewHolderPrint=holder;
		}

		holder.orderImageView.setVisibility(visibleType);
		holder.orderPriceTextView.setVisibility(visibleType);

		Picasso.get()
			.load(uri)
			.into(holder.orderImageView);
		holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
		holder.orderImageView.setOnClickListener(v -> {
			mainActivity.onItemClickImage(positionAdapter);
			mainActivity.onItemClickChanger();
		});

		holder.addOrderButton.setOnClickListener(v -> {
			mainActivity.onItemClickImage(positionAdapter);
			mainActivity.onItemClickChanger();
		});

		if (orders.get(positionAdapter).getUri().equals(defaultUri)) {
			holder.deleteOrderButton.setVisibility(View.GONE);
		} else {
			holder.deleteOrderButton.setVisibility(View.VISIBLE);
			holder.deleteOrderButton.setOnClickListener(v -> {
				mainActivity.onItemClickDelete(positionAdapter);
			});
		}

		holder.orderSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				holder.updateQualities(holder.sizes.get(position));
				setOrderDetails(holder, positionAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
			}
		});
		holder.orderQualitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				setOrderDetails(holder, positionAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});
		holder.orderQuantitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				setOrderDetails(holder, positionAdapter);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {

			}
		});

		holder.orderQuantitySpinner.setSelection(orders.get(positionAdapter).getQuantity() - 1, false);
		holder.orderQualitySpinner.setSelection(holder.qualities.indexOf(orders.get(positionAdapter).getQuality()), false);
		holder.orderSizeSpinner.setSelection(holder.sizes.indexOf(orders.get(positionAdapter).getSize()), false);
		setOrderDetails(holder, positionAdapter);
	}

	public OrderAdapterPrint.ViewHolderPrint getViewHolderPrint () {

		return (viewHolderPrint != null) ? viewHolderPrint : null;
	}

	private void setOrderDetails(ViewHolderPrint holder, int positionAdapter) {
		int sizePos = holder.orderSizeSpinner.getSelectedItemPosition();
		int qualityPos = holder.orderQualitySpinner.getSelectedItemPosition();

		float price = holder.priceMapSingleton.getPriceMap().getPriceBySelection(qualityPos, sizePos);

		orders.get(positionAdapter).setParams(
			holder.sizes.get(sizePos),
			holder.qualities.get(qualityPos),
			holder.orderQuantitySpinner.getSelectedItemPosition() + 1,
			price
		);
		holder.orderPriceTextView.setText(orders.get(positionAdapter).getSinglePrice());
		mainActivity.onItemClickChanger();
	}

	@Override
	public int getItemCount() {
		if (orders == null) return 0;
		return orders.size();
	}

	public void onAddNotify(List<Order> orders) {
		this.orders = orders;
		notifyDataSetChanged();
	}

	public class ViewHolderPrint extends RecyclerView.ViewHolder {

		private final ConstraintLayout orderBackground;

		private final TextView orderPriceTextView;

		private final ImageView orderImageView;

		private final Button addOrderButton;
		private final Button deleteOrderButton;

		private final Spinner orderSizeSpinner;
		private final Spinner orderQualitySpinner;
		private final Spinner orderQuantitySpinner;

		private final PriceMapSingleton priceMapSingleton;
		private final List<String> sizes;
		private final List<String> qualities;
		ArrayAdapter<String> sizeAdapter;
		ArrayAdapter<String> qualityAdapter;

		public ViewHolderPrint(View view) {
			super(view);

			orderBackground = view.findViewById(R.id.orderBackground);

			orderPriceTextView = view.findViewById(R.id.orderPriceTextView);

			orderImageView = view.findViewById(R.id.orderImageView);

			addOrderButton = view.findViewById(R.id.addOrderButton);
			deleteOrderButton = view.findViewById(R.id.deleteOrderButton);

			orderSizeSpinner = view.findViewById(R.id.orderSizeSpinner);
			orderQualitySpinner = view.findViewById(R.id.orderQualitySpinner);
			orderQuantitySpinner = view.findViewById(R.id.orderQuantitySpinner);

			priceMapSingleton = PriceMapSingleton.getInstance();

			sizes = new ArrayList<>();
			qualities = new ArrayList<>();

			sizes.addAll(priceMapSingleton.getPriceMap().getSizes());
			qualities.addAll(priceMapSingleton.getPriceMap().getQualities());

			sizeAdapter = new ArrayAdapter<>(mainActivity, R.layout.spinersize, sizes);
			sizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			orderSizeSpinner.setAdapter(sizeAdapter);

			qualityAdapter = new ArrayAdapter<>(mainActivity, R.layout.spinersize, qualities);
			qualityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			orderQualitySpinner.setAdapter(qualityAdapter);


			ArrayAdapter<CharSequence> quantityAdapter = ArrayAdapter.createFromResource(mainActivity,
				R.array.quantity_array, R.layout.spinersize);
			quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			orderQuantitySpinner.setAdapter(quantityAdapter);
		}

		private void updateQualities(String size) {
			int selectedPos = orderQualitySpinner.getSelectedItemPosition();
			String selectedQuality = qualities.get(orderQualitySpinner.getSelectedItemPosition());

			qualities.clear();
			qualities.addAll(priceMapSingleton.getPriceMap().getQualitiesBySize(size));

			if (qualities.contains(selectedQuality))
				orderQualitySpinner.setSelection(qualities.indexOf(selectedQuality));
			else if (selectedPos < qualities.size())
				orderQualitySpinner.setSelection(selectedPos);
			else orderQualitySpinner.setSelection(qualities.size() - 1);

			qualityAdapter.notifyDataSetChanged();
		}
	}
}
