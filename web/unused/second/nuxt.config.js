import colors from 'vuetify/es5/util/colors'

const isDev = process.env.NODE_ENV === 'development'

export default {
	ssr: true,
	target: 'static',
	srcDir: 'src_front/vue',
	buildDir: "../build/web_front",

	head: {
		titleTemplate: 'Улыбочку!',
		title: 'Улыбочку!',
		htmlAttrs: {
			lang: 'en'
		},
		meta: [
			{charset: 'utf-8'},
			{name: 'viewport', content: 'width=device-width, initial-scale=1'},
			{hid: 'description', name: 'description', content: ''}
		],
		link: [
			{rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
		]
	},

	components: true,

	modules: [
		'@nuxtjs/axios',
		'@nuxt/http',
		'@nuxtjs/apollo'
	],

	buildModules: [
		'@nuxt/typescript-build',
		'@nuxtjs/composition-api/module',
		'@nuxtjs/vuetify',
		'@nuxtjs/proxy'
	],

	http: {
		proxy: true
	},

	proxy: isDev ? [
		'http://localhost:8080/api_v3/**',
		// 'http://localhost:8080/login'
	] : undefined,

	vuetify: {
		theme: {
			dark: true,
			themes: {
				dark: {
					primary: colors.blue.darken2,
					accent: colors.grey.darken3,
					secondary: colors.amber.darken3,
					info: colors.teal.lighten1,
					warning: colors.amber.base,
					error: colors.deepOrange.accent4,
					success: colors.green.accent3
				}
			}
		}
	},

	build: {
		publicPath: '/_photoprint_/',
		cssSourceMap: isDev,
		devtools: isDev,
	},

	generate: {
		dir: 'src/main/resources/public',
		subFolders: false
	},

	server: {
		host: '0'
	}
}
