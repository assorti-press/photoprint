package by.dpi.photoprint_server.config;

import by.dpi.photoprint_server.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.google.protobuf.EnumValue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserService userService;

    public SecurityConfiguration(UserService userService) {
        this.userService = userService;
    }

    /*TESTING ONLY ENV
    *
    * test:test
    * SECURITY_USERNAME=test
    * SECURITY_PASSWORD=$2y$10$xO60oYDk065Dwtz9H0Xkm.3z0GJ8BbSYA96Co0ZcfWR4iseu4lKnO
    *
    * */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
				final String username = System.getenv("SECURITY_USERNAME");

				if(username == null)
					http
						.authorizeRequests()
							.antMatchers( "/**")
								.permitAll();
				else
					http
						.authorizeRequests()
							.antMatchers(
								"/_photoprint_/**",
								"/favicon.ico")
								.permitAll()
							.anyRequest()
								.authenticated()
							.and()
						.formLogin()
							.loginPage("/login")
								.permitAll()
							.successHandler(getAuthenticationSuccessHandler())
							.failureHandler(getAuthenticationFailureHandler())
							.and()
						.logout()
							.invalidateHttpSession(true)
							.clearAuthentication(true)
							.logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
							.logoutSuccessUrl("/login?logout")
								.permitAll();

					http.csrf().disable();
    }

   private AuthenticationSuccessHandler getAuthenticationSuccessHandler() {
      return (request, response, authentication) -> sendAuthJson(response, true);
   }

   private AuthenticationFailureHandler getAuthenticationFailureHandler(){
      return (request, response, exception) -> sendAuthJson(response, false);
   }

   private void sendAuthJson (HttpServletResponse response, boolean loginStatus)  throws IOException {
      Map<String, Boolean> responseJson = new HashMap<>();
      responseJson.put("login", loginStatus);

      ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
      String json = ow.writeValueAsString(responseJson);

      PrintWriter out = response.getWriter();
      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");
      out.print(json);
      out.flush();
   }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider auth = new DaoAuthenticationProvider();
        auth.setUserDetailsService(userService);
        auth.setPasswordEncoder(passwordEncoder());
        return auth;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }
}
