package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.auth.oauth2.GoogleCredentials;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.DefaultUriBuilderFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;

@Service
public class PhotoService {

	private static final String bucketName = System.getenv("BUCKET_NAME") != null ? System.getenv("BUCKET_NAME") : "";
	private static final String yandexOauth = System.getenv("YA_OAUTH") != null ? System.getenv("YA_OAUTH") : "";

	private static final String yandexUri = "https://cloud-api.yandex.net/v1/disk";
	private static final String googleUri = "https://storage.googleapis.com/storage/v1/b/" + bucketName + "/o";

	PhotoService() {
	}

	private ResponseEntity<String> yandexTemplate() {
		return yandexTemplate("");
	}

	private ResponseEntity<String> yandexTemplate(String path) {
		return yandexTemplate(path, HttpMethod.GET);
	}

	private ResponseEntity<String> yandexTemplate(String path, HttpMethod httpMethod) {
		try {
			return yandexTemplate(new URL(yandexUri + path), MediaType.ALL, httpMethod, String.class);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private ResponseEntity<byte[]> yandexImageTemplate(String url) {
		try {
			return yandexTemplate(new URL(url), MediaType.IMAGE_JPEG, HttpMethod.GET, byte[].class);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private <T> ResponseEntity<T> yandexTemplate(URL uri, MediaType mediaType, HttpMethod httpMethod, Class<T> responseType) {
		return responseTemplate(uri, "yandex", mediaType, httpMethod, responseType);
	}

	private ResponseEntity<String> googleTemplate(String dir) {
		return googleTemplate(dir, HttpMethod.GET);
	}

	private ResponseEntity<byte[]> googleImageTemplate(String url) {
		try {
			return googleTemplate(new URL(url), MediaType.IMAGE_JPEG, HttpMethod.GET, byte[].class);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return null;
	}

	private ResponseEntity<String> googleTemplate(String dir, HttpMethod httpMethod) {
		String query;
		if (!httpMethod.equals(HttpMethod.DELETE))
			query = "?" +
				((dir != null) ? "delimiter=" + URLEncoder.encode("/", StandardCharsets.UTF_8) : "") + "&" +
				((dir != null && dir.split("/").length > 0) ? "prefix=" + URLEncoder.encode(dir.replaceFirst("^/", "") + "/", StandardCharsets.UTF_8) : "");
		else query = "/" + URLEncoder.encode(dir.replaceAll("^/", ""), StandardCharsets.UTF_8).replace("+", "%20");

		try {
			URL uri = new URL(googleUri.replace("BUCKET_NAME", bucketName) + query);
			return googleTemplate(uri, MediaType.ALL, httpMethod, String.class);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		return null;
	}

	private <T> ResponseEntity<T> googleTemplate(URL uri, MediaType mediaType, HttpMethod httpMethod, Class<T> responseType) {
		return responseTemplate(uri, "google", mediaType, httpMethod, responseType);
	}

	private <T> ResponseEntity<T> responseTemplate(URL uri, String service, MediaType mediaType, HttpMethod httpMethod, Class<T> responseType) {
		try {
			String oAuth = "yandex".equals(service) ? "OAuth " + yandexOauth : "Bearer " + GoogleCredentials.getApplicationDefault().createScoped(
				Collections.singletonList(
					"https://www.googleapis.com/auth/devstorage.full_control"
				)
			).refreshAccessToken().getTokenValue();

			DefaultUriBuilderFactory defaultUriBuilderFactory = new DefaultUriBuilderFactory();
			defaultUriBuilderFactory.setEncodingMode(DefaultUriBuilderFactory.EncodingMode.NONE);

			RestTemplate restTemplate = new RestTemplate();
			restTemplate.setUriTemplateHandler(defaultUriBuilderFactory);

			HttpHeaders headers = new HttpHeaders();
			if (MediaType.IMAGE_JPEG.equals(mediaType)) {
				headers.setContentType(MediaType.IMAGE_JPEG);
				headers.setAccept(Collections.singletonList(MediaType.IMAGE_JPEG));
				headers.setCacheControl(CacheControl.noCache().getHeaderValue());
			} else
				headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
			headers.set("Authorization", oAuth);

			HttpEntity<String> entity = new HttpEntity<>(headers);

			return restTemplate.exchange(uri.toString(), httpMethod, entity, responseType);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	boolean getStatus() {
		ResponseEntity<String> responseEntity = yandexTemplate();
		return responseEntity.getStatusCodeValue() < 300;
	}

	YandexResources getFromYa(String path) throws JsonProcessingException {
		YandexResources yandexResources = null;
		if (getStatus()) {
			String uri = "/resources?" + "path=" + URLEncoder.encode(path, StandardCharsets.UTF_8)
				+ "&" + "limit=" + Long.MAX_VALUE;
			ResponseEntity<String> responseEntity = yandexTemplate(uri);
			ObjectMapper objectMapper = new ObjectMapper();
			yandexResources = objectMapper.readValue(responseEntity.getBody(), YandexResources.class);
		}
		return yandexResources;
	}

	Integer deleteFromYa(String path) {

		String uri = "/resources?" + "path=" + URLEncoder.encode(path, StandardCharsets.UTF_8)
			+ "&permanently=true";
		ResponseEntity<String> responseEntity = yandexTemplate(uri, HttpMethod.DELETE);

		if (responseEntity != null)
			return responseEntity.getStatusCodeValue();
		else return 1000;
	}

	CloudItems getFromGoogle(String dir) throws JsonProcessingException {

		ResponseEntity<String> responseEntity = googleTemplate(dir);
		ObjectMapper objectMapper = new ObjectMapper();

		if (responseEntity != null)
			return objectMapper.readValue(responseEntity.getBody(), CloudItems.class);
		else return null;
	}

	Integer deleteFromGoogle(String path, String type) {
		if ("dir".equals(type)) {
			HashMap<String, PhotoItem> photoItems = new HashMap<>();
			GoogleQuery(path.split("/$")[0], photoItems);
			photoItems.forEach((name, item) -> {
				if (item.getGoogleStorageItem() != null)
					deleteFromGoogle(item.getName(), item.getType());
			});
		}

		ResponseEntity<String> responseEntity = googleTemplate(
			path, HttpMethod.DELETE);

		if (responseEntity != null)
			return responseEntity.getStatusCodeValue();
		else return 1000;
	}

	PhotoItem photoItemGenerate(String unifiedName, String dir, String type,
															YandexDriveItem yandexDriveItem, GoogleStorageItem googleStorageItem) {
		PhotoItem photoItem = new PhotoItem();
		photoItem.setName(unifiedName);
		photoItem.setPath(dir);
		photoItem.setType(type);
		if (yandexDriveItem != null) photoItem.setYandexDriveItem(yandexDriveItem);
		if (googleStorageItem != null) photoItem.setGoogleStorageItem(googleStorageItem);

		return photoItem;
	}

	void yandexPhotoItemAdd(YaDriveItem yaItem, String dir, Map<String, PhotoItem> photoItems) {
		String unifiedName = yaItem.getPath()
			.replace("disk:/", "")
			.split((yaItem.getMimeType() != null && yaItem.getMediaType().equals("image")) ? ".jpeg" : "$")[0]
			.split("_\\d*+$")[0];

		YandexDriveItem yandexDriveItem = new YandexDriveItem();
		yandexDriveItem.setPath(yaItem.getPath());

		if (!yaItem.getType().equals("dir")) {
			yandexDriveItem.setMediaLink(yaItem.getFile());
			yandexDriveItem.setPreviewLink(yaItem.getPreview());
		}

		photoItemAdd(unifiedName, dir,
			(yaItem.getMediaType() != null && yaItem.getMediaType().contains("image")) ? "image" : yaItem.getType(),
			yandexDriveItem, null, photoItems);
	}

	void GooglePhotoItemAdd(Item item, String dir, Map<String, PhotoItem> photoItems) {
		String unifiedName = item.getName();

		GoogleStorageItem googleStorageItem = new GoogleStorageItem();
		googleStorageItem.setObjectId(item.getName());
		googleStorageItem.setMediaLink(item.getMediaLink());

		photoItemAdd(unifiedName, dir,
			(item.getContentType() != null && item.getContentType().contains("image")) ? "image" : "file",
			null, googleStorageItem, photoItems);
	}

	void GooglePhotoItemAdd(String prefix, String dir, Map<String, PhotoItem> photoItems) {
		String unifiedName = prefix.replaceFirst("/$", "");

		GoogleStorageItem googleStorageItem = new GoogleStorageItem();
		googleStorageItem.setObjectId(prefix);

		photoItemAdd(unifiedName, dir, "dir", null, googleStorageItem, photoItems);
	}

	void photoItemAdd(String name, String path, String type,
										YandexDriveItem yandexDriveItem, GoogleStorageItem googleStorageItem,
										Map<String, PhotoItem> photoItems) {
		photoItems.merge(name,
			photoItemGenerate(name, path, type, yandexDriveItem, googleStorageItem),
			(photoItemOld, photoItemNew) -> {
				if (yandexDriveItem != null) photoItemOld.setYandexDriveItem(yandexDriveItem);
				if (googleStorageItem != null) photoItemOld.setGoogleStorageItem(googleStorageItem);
				return photoItemOld;
			});
	}

	void YandexQuery(String dir, ConcurrentMap<String, PhotoItem> photoItems) {
		YandexResources yandexResources = null;
		try {
			yandexResources = getFromYa(dir);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		if (yandexResources != null && yandexResources.getEmbedded().getItems() != null)
			yandexResources.getEmbedded().getItems().forEach(yaDriveItem -> yandexPhotoItemAdd(yaDriveItem, dir, photoItems));
	}

	void GoogleQuery(String dir, Map<String, PhotoItem> photoItems) {
		CloudItems cloudItems = null;
		try {
			cloudItems = getFromGoogle(dir);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		if (cloudItems != null) {
			if (cloudItems.getPrefixes() != null)
				cloudItems.getPrefixes().forEach(element -> GooglePhotoItemAdd(element, dir, photoItems));
			if (cloudItems.getItems() != null)
				cloudItems.getItems().forEach(item -> GooglePhotoItemAdd(item, dir, photoItems));
		}
	}

	public Map<String, PhotoItem> getPhotoItems(String dir) throws InterruptedException {
		ConcurrentMap<String, PhotoItem> photoItems = new ConcurrentHashMap<>();

		ExecutorService es = Executors.newCachedThreadPool();
		es.execute(() -> YandexQuery(dir, photoItems));
		es.execute(() -> GoogleQuery(dir, photoItems));
		es.shutdown();

		return es.awaitTermination(2, TimeUnit.MINUTES) ? photoItems : null;
	}

	public Map<String, String> deletePhotoItem(String type, String path, String name) throws InterruptedException {

		Map<String, PhotoItem> photoItemMap = getPhotoItems(path);
		if (photoItemMap.containsKey(name) &&
			(
				"yandex".contains(type) && photoItemMap.get(name).getYandexDriveItem() != null
					&& deleteFromYa(photoItemMap.get(name).getYandexDriveItem().getPath()) < 300 ||
					"google".contains(type) && photoItemMap.get(name).getGoogleStorageItem() != null
						&& deleteFromGoogle(photoItemMap.get(name).getGoogleStorageItem().getObjectId(),
						photoItemMap.get(name).getType()) < 300
			)
		) return new HashMap<>() {{
				put("status", "ok");
			}};

		return new HashMap<>() {{
			put("status", "error");
		}};
	}


	public byte[] getImage(String path, String name) throws InterruptedException, IOException {
		return getImage(path, name, null);
	}
	public byte[] getImage(String path, String name, String size) throws InterruptedException, IOException {
		Map<String, PhotoItem> photoItemMap = getPhotoItems(path);

		if (photoItemMap.containsKey(name))
			if (photoItemMap.get(name).getYandexDriveItem() != null)
				if(size != null)
					return Objects.requireNonNull(yandexImageTemplate(
						photoItemMap.get(name).getYandexDriveItem().getPreviewLink().replaceAll("size=\\w+", "size=" + size)
					)).getBody();
				else return Objects.requireNonNull(yandexImageTemplate(
					photoItemMap.get(name).getYandexDriveItem().getMediaLink())).getBody();
			else if (photoItemMap.get(name).getGoogleStorageItem() != null) {
				byte[] image = Objects.requireNonNull(googleImageTemplate(photoItemMap.get(name).getGoogleStorageItem().getMediaLink())).getBody();
				return size == null ? image : resize(image,
					Integer.parseInt(size.replaceAll("x\\d+$", "")),
					Integer.parseInt(size.replaceAll("^\\d+x", "")));
			}

		return null;
	}

	private byte[] resize(byte[] inputImage, int scaledWidth, int scaledHeight) throws IOException {
		InputStream is = new ByteArrayInputStream(inputImage);
		BufferedImage inputBufferedImage = ImageIO.read(is);

		double horizCoef = (double) scaledWidth / (double) inputBufferedImage.getRaster().getWidth();
		double vertCoef = (double) scaledHeight / (double) inputBufferedImage.getRaster().getHeight();
		int inputWidth = (horizCoef < vertCoef) ?
			scaledWidth : (int) Math.round(inputBufferedImage.getRaster().getWidth() * vertCoef);
		int inputHeight = (horizCoef > vertCoef) ?
			scaledHeight : (int) Math.round(inputBufferedImage.getRaster().getHeight() * horizCoef);

		BufferedImage outputBufferedImage = new BufferedImage(inputWidth,
			inputHeight, inputBufferedImage.getType());

		Graphics2D g2d = outputBufferedImage.createGraphics();
		g2d.drawImage(inputBufferedImage, 0, 0, inputWidth,
			inputHeight, null);
		g2d.dispose();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write(outputBufferedImage, "jpg", baos);

		return baos.toByteArray();
	}
}
