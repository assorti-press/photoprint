package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.MenuItem;
import com.google.api.core.ApiFuture;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.*;
import com.google.cloud.firestore.EventListener;
import com.google.common.collect.Iterators;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.annotations.Nullable;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class MenuFirestoreService {
	private static final String COL_NAME = System.getenv("DATABASE_COL_MENU") == null ?
		"Menu" : System.getenv("DATABASE_COL_MENU");

	private final CollectionReference orderFirestoreReference;

	MenuFirestoreService(FirebaseInitService firebaseInitService) {
		FirebaseDatabase database = firebaseInitService.getDatabase();
		Firestore firestore = firebaseInitService.getFirestore();

		orderFirestoreReference = firestore.collection(COL_NAME);

		setListener(orderFirestoreReference,
			database.getReference(COL_NAME),
			Comparator.comparing(MenuItem::getOrderPrior),
			MenuItem.class);
	}

	public <T> void setListener(CollectionReference orderFirestoreReference,
															DatabaseReference databaseReference,
															Comparator<T> comparator,
															@Nonnull Class<T> valueType) {
		orderFirestoreReference.addSnapshotListener((snapshots, e) -> {
			if (e != null) {
				System.err.println("Listen failed:" + e);
				return;
			}
			if (snapshots != null) {
				List<T> itemList = new ArrayList<>();

				for(QueryDocumentSnapshot documentSnapshot : snapshots.getDocuments()){
					itemList.add(documentSnapshot.toObject(valueType));
				}

				itemList.sort(comparator);
				databaseReference.setValueAsync(itemList);
			}
		});
	}

	public MenuItem getItem(String name) throws ExecutionException, InterruptedException {
		return orderFirestoreReference.document(name).get().get().toObject(MenuItem.class);
	}

	public ArrayList<MenuItem> getList(){
		ArrayList<MenuItem> menuItemList = new ArrayList<>();
		orderFirestoreReference.listDocuments().forEach((menuItemReference) -> {
			try {
				MenuItem menuItem = menuItemReference.get().get().toObject(MenuItem.class);
				if (menuItem != null) {
					menuItem.setName(menuItemReference.getId());
					menuItemList.add(menuItem);
				}

			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		});
		return menuItemList;
	}

	public void setList(List<MenuItem> list) throws ExecutionException, InterruptedException {
		for(MenuItem menuItem : list){
			orderFirestoreReference.document(menuItem.getName()).set(menuItem);
		}
	}

	public boolean updateList(List<MenuItem> list) throws ExecutionException, InterruptedException {
		boolean interruption = false;
		for(MenuItem menuItem : list){
			interruption = interruption || orderFirestoreReference.document(menuItem.getName()).update("orderPrior", menuItem.getOrderPrior()).isCancelled();
		}
		return !interruption;
	}

	public boolean insertItem(String name, MenuItem item) throws ExecutionException, InterruptedException {
		if(!orderFirestoreReference.document(name).get().get().exists()) {
			return !orderFirestoreReference.document(name).set(item).isCancelled();
		}
		return false;
	}

	public boolean updateItem(String name, MenuItem item) throws ExecutionException, InterruptedException {
		Map<String, Object> fields = new HashMap<>();
		fields.put("name", name);
		fields.put("name_ru", item.getName_ru());
		fields.put("html", item.getHtml());

		return !orderFirestoreReference.document(name).update(fields).isCancelled();
	}

	public boolean deleteItem(String name) {
		return !orderFirestoreReference.document(name).delete().isCancelled();
	}
}
