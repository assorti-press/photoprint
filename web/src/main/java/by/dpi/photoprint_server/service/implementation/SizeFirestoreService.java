package by.dpi.photoprint_server.service.implementation;

import by.dpi.photoprint_server.model.Size;
import by.dpi.photoprint_server.service.FirebaseInitService;
import by.dpi.photoprint_server.service.PricesFirestoreService;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class SizeFirestoreService extends PricesFirestoreService<Size> {
	private static final String COL_NAME = System.getenv("DATABASE_COL_PRICE") == null ?
		"Prices" : System.getenv("DATABASE_COL_PRICE");

	private final DocumentReference sizeFirestoreReference;

	SizeFirestoreService(FirebaseInitService firebaseInitService) {
		Firestore firestore = firebaseInitService.getFirestore();
		sizeFirestoreReference = firestore.collection(COL_NAME).document("sizes");
	}

	@Override
	public List<Size> get() throws ExecutionException, InterruptedException {
		return getList(sizeFirestoreReference);
	}

	@Override
	public void set(List<Size> list) {
		setList(sizeFirestoreReference, list);
	}
}
