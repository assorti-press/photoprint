package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
public class NewUserService {

    public static final String COL_NAME="registered";

    public void saveUserDetails(User user, String UID){
        Firestore dbFirestore = FirestoreClient.getFirestore();
        dbFirestore.collection(COL_NAME).document(UID).set(user);
    }

    public boolean isUserExists(String UID) throws ExecutionException, InterruptedException {
        Firestore firestore = FirestoreClient.getFirestore();
        ApiFuture<DocumentSnapshot> documentSnapshotApiFuture = firestore.collection(COL_NAME).document(UID).get();

        return documentSnapshotApiFuture.get().exists();
    }
}