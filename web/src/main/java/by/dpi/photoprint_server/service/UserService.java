package by.dpi.photoprint_server.service;

import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

}
