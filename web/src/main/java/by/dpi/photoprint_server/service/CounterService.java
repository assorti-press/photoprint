package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.Counter;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.cloud.FirestoreClient;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
public class CounterService {

    public static final String COL_NAME="counter";

    public long getCounterDetails() throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        DocumentReference documentReference = dbFirestore.collection(COL_NAME).document("0");
        ApiFuture<DocumentSnapshot> future = documentReference.get();

        DocumentSnapshot document = future.get();

        if(document.exists()) {
            Counter counter = document.toObject(Counter.class);
            return counter != null ? counter.getCount() : 0L;
        } else {
            return 0L;
        }
    }

    public void updateCounterDetails(long count) {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        dbFirestore.collection(COL_NAME).document("0").set(new Counter(count));
    }
}