package by.dpi.photoprint_server.service.implementation;

import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

import by.dpi.photoprint_server.service.UserService;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import by.dpi.photoprint_server.model.Role;

@Service
public class UserServiceImp implements UserService {
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        if (!email.equals(System.getenv("SECURITY_USERNAME"))){
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return new org.springframework.security.core.userdetails.User(System.getenv("SECURITY_USERNAME"),
                System.getenv("SECURITY_PASSWORD"),
                mapRolesToAuthorities(Collections.singletonList(new Role("ROLE_ADMIN"))));
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles){
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }
}
