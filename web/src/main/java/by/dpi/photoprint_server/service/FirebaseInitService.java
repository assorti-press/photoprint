package by.dpi.photoprint_server.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;
import com.google.firebase.database.*;
import org.springframework.stereotype.Service;

@Service
public class FirebaseInitService  {

   FirebaseInitService() {
      initialize();
   }

    public void initialize() {
        try {
            FirebaseOptions options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.getApplicationDefault())
                    .setDatabaseUrl(System.getenv("DATABASE_URL"))
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   public FirebaseDatabase getDatabase(){
      return FirebaseDatabase.getInstance();
   }

   public Firestore getFirestore() {
      return FirestoreClient.getFirestore();
   }
}
