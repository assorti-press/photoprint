package by.dpi.photoprint_server.service;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;

@Service
public class TelegramInitService {
    @PostConstruct
    public void initialize()  {
        try {
			TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
			botsApi.registerBot(new TelegramService());
		} catch (TelegramApiException e) {
			e.printStackTrace();
		}
    }
}