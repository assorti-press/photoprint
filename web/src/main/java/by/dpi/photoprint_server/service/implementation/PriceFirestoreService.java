package by.dpi.photoprint_server.service.implementation;

import by.dpi.photoprint_server.model.MenuItem;
import by.dpi.photoprint_server.model.PriceList;
import by.dpi.photoprint_server.service.FirebaseInitService;
import by.dpi.photoprint_server.service.PricesFirestoreService;
import com.google.cloud.firestore.*;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.stereotype.Service;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class PriceFirestoreService extends PricesFirestoreService<PriceList> {
	private static final String COL_NAME = System.getenv("DATABASE_COL_PRICE") == null ?
		"Prices" : System.getenv("DATABASE_COL_PRICE");

	private final DatabaseReference orderDatabaseReference;
	private final DocumentReference orderFirestoreReference;

	PriceFirestoreService(FirebaseInitService firebaseInitService) {
		FirebaseDatabase database = firebaseInitService.getDatabase();
		Firestore firestore = firebaseInitService.getFirestore();

		orderDatabaseReference = database.getReference(COL_NAME);

		orderFirestoreReference = firestore.collection(COL_NAME).document("prices");
	}

	@Override
	public List<PriceList> get() throws ExecutionException, InterruptedException {
		return getList(orderFirestoreReference);
	}

	@Override
	public void set(List<PriceList> list) {
		orderDatabaseReference.setValueAsync(list);
		setList(orderFirestoreReference, list);
	}
}
