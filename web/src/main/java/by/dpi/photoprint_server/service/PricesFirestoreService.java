package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.*;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.DocumentSnapshot;
import static org.springframework.data.util.CastUtils.cast;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public abstract class PricesFirestoreService<T> {

	public abstract List<T> get() throws ExecutionException, InterruptedException;
	public abstract void set(List<T> list);

	public List<T> getList(DocumentReference firestoreReference) throws ExecutionException, InterruptedException {
		DocumentSnapshot documentSnapshot = firestoreReference.get().get();

		if (documentSnapshot.exists()) {
			Object object = documentSnapshot.get("array");
			if (object != null)
				return cast(object);
		}

		return new ArrayList<>();
	}

	public void setList(DocumentReference firestoreReference, List<T> list) {
		firestoreReference.set(new FirestoreWrap<T>(list));
	}
}
