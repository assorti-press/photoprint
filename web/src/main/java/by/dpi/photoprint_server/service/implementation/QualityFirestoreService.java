package by.dpi.photoprint_server.service.implementation;

import by.dpi.photoprint_server.model.Quality;
import by.dpi.photoprint_server.service.FirebaseInitService;
import by.dpi.photoprint_server.service.PricesFirestoreService;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.firebase.database.FirebaseDatabase;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service
public class QualityFirestoreService extends PricesFirestoreService<Quality> {
	private static final String COL_NAME = System.getenv("DATABASE_COL_PRICE") == null ?
		"Prices" : System.getenv("DATABASE_COL_PRICE");

	private final DocumentReference qualityFirestoreReference;

	QualityFirestoreService(FirebaseInitService firebaseInitService) {
		FirebaseDatabase database = firebaseInitService.getDatabase();
		Firestore firestore = firebaseInitService.getFirestore();

		qualityFirestoreReference = firestore.collection(COL_NAME).document("quality");
	}

	@Override
	public List<Quality> get() throws ExecutionException, InterruptedException {
		return getList(qualityFirestoreReference);
	}

	@Override
	public void set(List<Quality> list) {
		setList(qualityFirestoreReference, list);
	}
}
