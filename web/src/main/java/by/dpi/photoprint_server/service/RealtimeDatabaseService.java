package by.dpi.photoprint_server.service;

import by.dpi.photoprint_server.model.Order;
import by.dpi.photoprint_server.model.User;
import com.google.firebase.database.*;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

@Service
public class RealtimeDatabaseService {
   final TelegramService telegramService;
   final NewUserService newUserService;

   final FirebaseDatabase database;

   final DatabaseReference orderReference;
   final DatabaseReference userReference;

   final Map<String, Timer> sendSchedule;

   RealtimeDatabaseService(TelegramService telegramService, NewUserService newUserService) {
      this.telegramService = telegramService;
      this.newUserService = newUserService;

      this.database = FirebaseDatabase.getInstance();

      this.orderReference = database.getReference(System.getenv("DATABASE_COL_ORDERS"));
      this.userReference = database.getReference(System.getenv("DATABASE_COL_USERS"));

      this.sendSchedule = new HashMap<>();
   }

   @PostConstruct
   public void setListener() {
      orderReference.addChildEventListener(new ChildEventListener() {
         @Override
         public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {

               onOrderChildAdded(dataSnapshot);


            orderReference.child(dataSnapshot.getKey()).setValueAsync(null);
         }

         @Override
         public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
         }

         @Override
         public void onChildRemoved(DataSnapshot dataSnapshot) {
         }

         @Override
         public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
         }

         @Override
         public void onCancelled(DatabaseError databaseError) {
         }
      });
      userReference.addChildEventListener(new ChildEventListener() {
         @Override
         public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
            try {
               onUserChildAdded(snapshot);
            } catch (ExecutionException | InterruptedException e) {
               e.printStackTrace();
            }
            userReference.child(snapshot.getKey()).setValueAsync(null);
         }

         @Override
         public void onChildChanged(DataSnapshot snapshot, String previousChildName) {

         }

         @Override
         public void onChildRemoved(DataSnapshot snapshot) {

         }

         @Override
         public void onChildMoved(DataSnapshot snapshot, String previousChildName) {

         }

         @Override
         public void onCancelled(DatabaseError error) {

         }
      });
   }

   void onOrderChildAdded(DataSnapshot dataSnapshot) {
      Order order = dataSnapshot.getValue(Order.class);

      Timer scheduleTimer = sendSchedule.get(order.getPhone());
      if(scheduleTimer != null) scheduleTimer.cancel();

      Timer timer = new Timer();
      timer.schedule(new TimerTask() {
         @Override
         public void run() {
            try {
               telegramService.sendNewOrderMessage(order);
               sendSchedule.remove(order.getPhone());
            } catch (Exception e) {
               e.printStackTrace();
            }
         }
      }, 60000L);

      sendSchedule.put(order.getPhone(), timer);
   }

   void onUserChildAdded(DataSnapshot dataSnapshot) throws ExecutionException, InterruptedException {
      User user = dataSnapshot.getValue(User.class);

      if (!user.getOldPhoneNumber().isEmpty())
         telegramService.sendNumberChangeUserMessage(user);

      if (!newUserService.isUserExists(user.getUID())) {
         newUserService.saveUserDetails(dataSnapshot.getValue(User.class), user.getUID());
         telegramService.sendNewUserMessage(user);
      }
   }
}
