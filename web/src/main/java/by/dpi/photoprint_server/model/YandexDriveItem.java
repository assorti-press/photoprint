package by.dpi.photoprint_server.model;

public class YandexDriveItem {
	String previewLink;
	String mediaLink;
	String path;

	public YandexDriveItem() {
	}

	public YandexDriveItem(String previewLink, String mediaLink, String path) {
		this.previewLink = previewLink;
		this.mediaLink = mediaLink;
		this.path = path;
	}

	public String getPreviewLink() {
		return previewLink;
	}

	public void setPreviewLink(String previewLink) {
		this.previewLink = previewLink;
	}

	public String getMediaLink() {
		return mediaLink;
	}

	public void setMediaLink(String mediaLink) {
		this.mediaLink = mediaLink;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}
}
