package by.dpi.photoprint_server.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"items",
	"total"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("unused")
public class Embedded {

	@JsonProperty("items")
	private List<YaDriveItem> yaDriveItems = null;
	@JsonProperty("total")
	private Long total;

	public Embedded() {
	}

	public Embedded(List<YaDriveItem> yaDriveItems, Long total) {
		super();
		this.yaDriveItems = yaDriveItems;
		this.total = total;
	}

	@JsonProperty("items")
	public List<YaDriveItem> getItems() {
		return yaDriveItems;
	}

	@JsonProperty("items")
	public void setItems(List<YaDriveItem> yaDriveItems) {
		this.yaDriveItems = yaDriveItems;
	}

	@JsonProperty("total")
	public Long getTotal() {
		return total;
	}

	@JsonProperty("total")
	public void setTotal(Long total) {
		this.total = total;
	}
}
