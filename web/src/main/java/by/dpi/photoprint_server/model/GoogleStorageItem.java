package by.dpi.photoprint_server.model;

public class GoogleStorageItem {
	String objectId;
	String mediaLink;

	public GoogleStorageItem() {
	}

	public GoogleStorageItem(String objectId, String mediaLink) {
		this.objectId = objectId;
		this.mediaLink = mediaLink;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getMediaLink() {
		return mediaLink;
	}

	public void setMediaLink(String mediaLink) {
		this.mediaLink = mediaLink;
	}
}
