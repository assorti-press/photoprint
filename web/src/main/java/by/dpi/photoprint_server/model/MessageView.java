package by.dpi.photoprint_server.model;

public final class MessageView {
    public interface id {}
    public interface text extends id {}
    public interface full extends text {}
}
