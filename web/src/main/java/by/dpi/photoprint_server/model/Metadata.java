
package by.dpi.photoprint_server.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "firebaseStorageDownloadTokens"
})
@Generated("jsonschema2pojo")
public class Metadata {

    @JsonProperty("firebaseStorageDownloadTokens")
    private String firebaseStorageDownloadTokens;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("firebaseStorageDownloadTokens")
    public String getFirebaseStorageDownloadTokens() {
        return firebaseStorageDownloadTokens;
    }

    @JsonProperty("firebaseStorageDownloadTokens")
    public void setFirebaseStorageDownloadTokens(String firebaseStorageDownloadTokens) {
        this.firebaseStorageDownloadTokens = firebaseStorageDownloadTokens;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
