package by.dpi.photoprint_server.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

@SuppressWarnings("unused")
public class PhotoItem implements Comparable<PhotoItem> {
	String name;
	String size = "—";
	String type;
	String modified = "—";
	String path;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("yandexObject")
	YandexDriveItem yandexDriveItem;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	@JsonProperty("googleObject")
	GoogleStorageItem googleStorageItem;

	public PhotoItem() {
	}

	public PhotoItem(String name, String size, String type, String modified, String path, YandexDriveItem yandexDriveItem, GoogleStorageItem googleStorageItem) {
		this.name = name;
		this.size = size;
		this.type = type;
		this.modified = modified;
		this.path = path;
		this.yandexDriveItem = yandexDriveItem;
		this.googleStorageItem = googleStorageItem;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSize() {
		return size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@JsonProperty("yandexObject")
	public YandexDriveItem getYandexDriveItem() {
		return yandexDriveItem;
	}

	@JsonProperty("yandexObject")
	public void setYandexDriveItem(YandexDriveItem yandexDriveItem) {
		this.yandexDriveItem = yandexDriveItem;
	}

	@JsonProperty("googleObject")
	public GoogleStorageItem getGoogleStorageItem() {
		return googleStorageItem;
	}

	@JsonProperty("googleObject")
	public void setGoogleStorageItem(GoogleStorageItem googleStorageItem) {
		this.googleStorageItem = googleStorageItem;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		PhotoItem photoItem = (PhotoItem) o;
		return name.equals(photoItem.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public int compareTo(PhotoItem photoItem) {
		return this.name.compareTo(photoItem.name);
	}
}
