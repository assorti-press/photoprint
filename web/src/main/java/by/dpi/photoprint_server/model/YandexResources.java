
package by.dpi.photoprint_server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"_embedded",
	"type",
	"path",
	"created",
	"modified",
})
@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("unused")
public class YandexResources {

	@JsonProperty("_embedded")
	private Embedded embedded;
	@JsonProperty("type")
	private String type;
	@JsonProperty("path")
	private String path;
	@JsonProperty("created")
	private String created;
	@JsonProperty("modified")
	private String modified;

	public YandexResources() {
	}

	public YandexResources(Embedded embedded, String type, String path, String created, String modified) {
		super();
		this.embedded = embedded;
		this.type = type;
		this.path = path;
		this.created = created;
		this.modified = modified;
	}

	@JsonProperty("_embedded")
	public Embedded getEmbedded() {
		return embedded;
	}

	@JsonProperty("_embedded")
	public void setEmbedded(Embedded embedded) {
		this.embedded = embedded;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("path")
	public String getPath() {
		return path;
	}

	@JsonProperty("path")
	public void setPath(String path) {
		this.path = path;
	}

	@JsonProperty("created")
	public String getCreated() {
		return created;
	}

	@JsonProperty("created")
	public void setCreated(String created) {
		this.created = created;
	}

	@JsonProperty("modified")
	public String getModified() {
		return modified;
	}

	@JsonProperty("modified")
	public void setModified(String modified) {
		this.modified = modified;
	}
}
