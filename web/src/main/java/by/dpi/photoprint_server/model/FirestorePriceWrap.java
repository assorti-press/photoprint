package by.dpi.photoprint_server.model;

import java.util.List;

public class FirestorePriceWrap {
   List<PriceList> array;

   public FirestorePriceWrap() {
   }

   public FirestorePriceWrap(List<PriceList> array) {
      this.array = array;
   }

   public List<PriceList> getPrices() {
      return array;
   }

   public void setPrices(List<PriceList> array) {
      this.array = array;
   }
}
