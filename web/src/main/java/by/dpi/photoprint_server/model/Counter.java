package by.dpi.photoprint_server.model;

public class Counter {
    long count;

    public Counter() {
    }

    public Counter(long count) {
        this.count = count;
    }

    public long getCount() {
        return count;
    }
}
