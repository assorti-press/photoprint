package by.dpi.photoprint_server.model;

import java.util.List;

public class FirestoreWrap<T> {
   	List<T> array;

   public FirestoreWrap(List<T> array) {
      this.array = array;
   }

   public List<T> getArray() {
      return array;
   }

   public void setArray(List<T> array) {
      this.array = array;
   }

	}
