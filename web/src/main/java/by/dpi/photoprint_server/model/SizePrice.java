package by.dpi.photoprint_server.model;

import java.util.Objects;

public class SizePrice {
   Size size;
   double price;

   public SizePrice() {
   }

   public SizePrice(Size size, double price) {
      this.size = size;
      this.price = price;
   }

   public Size getSize() {
      return size;
   }

   public void setSize(Size size) {
      this.size = size;
   }

   public double getPrice() {
      return price;
   }

   public void setPrice(double price) {
      this.price = price;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      SizePrice sizePrice = (SizePrice) o;
      return size.equals(sizePrice.size);
   }

   @Override
   public int hashCode() {
      return Objects.hash(size);
   }
}
