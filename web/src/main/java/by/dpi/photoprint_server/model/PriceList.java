package by.dpi.photoprint_server.model;

import java.util.List;
import java.util.Set;

public class PriceList {
   Quality quality;
   List<SizePrice> sizePrices;

   public PriceList() {
   }

   public PriceList(Quality quality, List<SizePrice> sizePrices) {
      this.quality = quality;
      this.sizePrices = sizePrices;
   }

   public Quality getQuality() {
      return quality;
   }

   public void setQuality(Quality quality) {
      this.quality = quality;
   }

   public List<SizePrice> getSizePrices() {
      return sizePrices;
   }

   public void setSizePrices(List<SizePrice> sizePrices) {
      this.sizePrices = sizePrices;
   }
}
