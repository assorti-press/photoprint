package by.dpi.photoprint_server.model;

import java.util.List;

public class FirestoreQualityWrap {
   List<Quality> array;

   public FirestoreQualityWrap() {
   }

   public FirestoreQualityWrap(List<Quality> array) {
      this.array = array;
   }

   public List<Quality> getQualities() {
      return array;
   }

   public void setQualities(List<Quality> array) {
      this.array = array;
   }
}
