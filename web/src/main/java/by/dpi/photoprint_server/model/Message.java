package by.dpi.photoprint_server.model;

import com.fasterxml.jackson.annotation.JsonTypeId;
import com.fasterxml.jackson.annotation.JsonView;

import java.time.LocalDateTime;

public class Message {
    @JsonView(MessageView.id.class)
    private String id;
    @JsonView(MessageView.text.class)
    private String text;
    @JsonView(MessageView.full.class)
    private LocalDateTime localDateTime;

    public Message() {
    }

    public Message(String id, String text) {
        this.id = id;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDateTime getLocalDateTime() {
        return localDateTime;
    }

    public void setLocalDateTime(LocalDateTime localDateTime) {
        this.localDateTime = localDateTime;
    }
}
