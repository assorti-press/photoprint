package by.dpi.photoprint_server.model;

import java.util.List;

public class PhotoItems {

	private List<String> yaPhotos;

	public PhotoItems() {
	}

	public List<String> getYaPhotos() {
		return yaPhotos;
	}

	public void setYaPhotos(List<String> yaPhotos) {
		this.yaPhotos = yaPhotos;
	}
}
