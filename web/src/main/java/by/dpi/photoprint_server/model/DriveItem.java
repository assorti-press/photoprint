package by.dpi.photoprint_server.model;

@SuppressWarnings("unused")
public class DriveItem {
	private String name;
	private String file;
	private String preview;
	private Long size;
	private String type;
	private String mimeType;
	private String mediaType;
	private String path;
	private String created;
	private String modified;
	private String sha256;
	private String md5;

	public DriveItem() {
	}

	public DriveItem(String name, String file, String preview, Long size, String type, String mimeType, String mediaType, String path, String created, String modified, String sha256, String md5) {
		this.name = name;
		this.file = file;
		this.preview = preview;
		this.size = size;
		this.type = type;
		this.mimeType = mimeType;
		this.mediaType = mediaType;
		this.path = path;
		this.created = created;
		this.modified = modified;
		this.sha256 = sha256;
		this.md5 = md5;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getPreview() {
		return preview;
	}

	public void setPreview(String preview) {
		this.preview = preview;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getModified() {
		return modified;
	}

	public void setModified(String modified) {
		this.modified = modified;
	}

	public String getSha256() {
		return sha256;
	}

	public void setSha256(String sha256) {
		this.sha256 = sha256;
	}

	public String getMd5() {
		return md5;
	}

	public void setMd5(String md5) {
		this.md5 = md5;
	}
}
