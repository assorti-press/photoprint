package by.dpi.photoprint_server.model;

import java.util.List;

public class FirestoreSizeWrap {
   List<Size> array;

   public FirestoreSizeWrap() {
   }

   public FirestoreSizeWrap(List<Size> array) {
      this.array = array;
   }

   public List<Size> getSizes() {
      return array;
   }

   public void setSizes(List<Size> array) {
      this.array = array;
   }
}
