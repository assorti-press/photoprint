
package by.dpi.photoprint_server.model;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "kind",
    "id",
    "selfLink",
    "mediaLink",
    "name",
    "bucket",
    "generation",
    "metageneration",
    "contentType",
    "storageClass",
    "size",
    "md5Hash",
    "contentDisposition",
    "crc32c",
    "etag",
    "timeCreated",
    "updated",
    "timeStorageClassUpdated",
    "metadata"
})
@Generated("jsonschema2pojo")
public class Item {

    @JsonProperty("kind")
    private String kind;
    @JsonProperty("id")
    private String id;
    @JsonProperty("selfLink")
    private String selfLink;
    @JsonProperty("mediaLink")
    private String mediaLink;
    @JsonProperty("name")
    private String name;
    @JsonProperty("bucket")
    private String bucket;
    @JsonProperty("generation")
    private String generation;
    @JsonProperty("metageneration")
    private String metageneration;
    @JsonProperty("contentType")
    private String contentType;
    @JsonProperty("storageClass")
    private String storageClass;
    @JsonProperty("size")
    private String size;
    @JsonProperty("md5Hash")
    private String md5Hash;
    @JsonProperty("contentDisposition")
    private String contentDisposition;
    @JsonProperty("crc32c")
    private String crc32c;
    @JsonProperty("etag")
    private String etag;
    @JsonProperty("timeCreated")
    private String timeCreated;
    @JsonProperty("updated")
    private String updated;
    @JsonProperty("timeStorageClassUpdated")
    private String timeStorageClassUpdated;
    @JsonProperty("metadata")
    private Metadata metadata;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("kind")
    public String getKind() {
        return kind;
    }

    @JsonProperty("kind")
    public void setKind(String kind) {
        this.kind = kind;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("selfLink")
    public String getSelfLink() {
        return selfLink;
    }

    @JsonProperty("selfLink")
    public void setSelfLink(String selfLink) {
        this.selfLink = selfLink;
    }

    @JsonProperty("mediaLink")
    public String getMediaLink() {
        return mediaLink;
    }

    @JsonProperty("mediaLink")
    public void setMediaLink(String mediaLink) {
        this.mediaLink = mediaLink;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("bucket")
    public String getBucket() {
        return bucket;
    }

    @JsonProperty("bucket")
    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    @JsonProperty("generation")
    public String getGeneration() {
        return generation;
    }

    @JsonProperty("generation")
    public void setGeneration(String generation) {
        this.generation = generation;
    }

    @JsonProperty("metageneration")
    public String getMetageneration() {
        return metageneration;
    }

    @JsonProperty("metageneration")
    public void setMetageneration(String metageneration) {
        this.metageneration = metageneration;
    }

    @JsonProperty("contentType")
    public String getContentType() {
        return contentType;
    }

    @JsonProperty("contentType")
    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @JsonProperty("storageClass")
    public String getStorageClass() {
        return storageClass;
    }

    @JsonProperty("storageClass")
    public void setStorageClass(String storageClass) {
        this.storageClass = storageClass;
    }

    @JsonProperty("size")
    public String getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(String size) {
        this.size = size;
    }

    @JsonProperty("md5Hash")
    public String getMd5Hash() {
        return md5Hash;
    }

    @JsonProperty("md5Hash")
    public void setMd5Hash(String md5Hash) {
        this.md5Hash = md5Hash;
    }

    @JsonProperty("contentDisposition")
    public String getContentDisposition() {
        return contentDisposition;
    }

    @JsonProperty("contentDisposition")
    public void setContentDisposition(String contentDisposition) {
        this.contentDisposition = contentDisposition;
    }

    @JsonProperty("crc32c")
    public String getCrc32c() {
        return crc32c;
    }

    @JsonProperty("crc32c")
    public void setCrc32c(String crc32c) {
        this.crc32c = crc32c;
    }

    @JsonProperty("etag")
    public String getEtag() {
        return etag;
    }

    @JsonProperty("etag")
    public void setEtag(String etag) {
        this.etag = etag;
    }

    @JsonProperty("timeCreated")
    public String getTimeCreated() {
        return timeCreated;
    }

    @JsonProperty("timeCreated")
    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    @JsonProperty("updated")
    public String getUpdated() {
        return updated;
    }

    @JsonProperty("updated")
    public void setUpdated(String updated) {
        this.updated = updated;
    }

    @JsonProperty("timeStorageClassUpdated")
    public String getTimeStorageClassUpdated() {
        return timeStorageClassUpdated;
    }

    @JsonProperty("timeStorageClassUpdated")
    public void setTimeStorageClassUpdated(String timeStorageClassUpdated) {
        this.timeStorageClassUpdated = timeStorageClassUpdated;
    }

    @JsonProperty("metadata")
    public Metadata getMetadata() {
        return metadata;
    }

    @JsonProperty("metadata")
    public void setMetadata(Metadata metadata) {
        this.metadata = metadata;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
