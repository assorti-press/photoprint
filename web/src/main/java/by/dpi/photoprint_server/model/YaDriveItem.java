package by.dpi.photoprint_server.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
	"file",
	"preview",
	"size",
	"type",
	"mime_type",
	"media_type",
	"path",
	"created",
	"modified",
	"sha256",
	"md5"
})
@JsonIgnoreProperties(ignoreUnknown = true)
@SuppressWarnings("unused")
public class YaDriveItem {
	@JsonProperty("file")
	private String file;
	@JsonProperty("preview")
	private String preview;
	@JsonProperty("size")
	private Long size;
	@JsonProperty("type")
	private String type;
	@JsonProperty("mime_type")
	private String mimeType;
	@JsonProperty("media_type")
	private String mediaType;
	@JsonProperty("path")
	private String path;
	@JsonProperty("created")
	private String created;
	@JsonProperty("modified")
	private String modified;
	@JsonProperty("sha256")
	private String sha256;
	@JsonProperty("md5")
	private String md5;

	public YaDriveItem() {
	}

	public YaDriveItem(String file, String preview, Long size, String type, String mimeType, String mediaType, String path, String created, String modified, String sha256, String md5) {
		this.file = file;
		this.preview = preview;
		this.size = size;
		this.type = type;
		this.mimeType = mimeType;
		this.mediaType = mediaType;
		this.path = path;
		this.created = created;
		this.modified = modified;
		this.sha256 = sha256;
		this.md5 = md5;
	}

	@JsonProperty("created")
	public String getCreated() {
		return created;
	}

	@JsonProperty("created")
	public void setCreated(String created) {
		this.created = created;
	}

	@JsonProperty("size")
	public Long getSize() {
		return size;
	}

	@JsonProperty("size")
	public void setSize(Long size) {
		this.size = size;
	}

	@JsonProperty("modified")
	public String getModified() {
		return modified;
	}

	@JsonProperty("modified")
	public void setModified(String modified) {
		this.modified = modified;
	}

	@JsonProperty("mime_type")
	public String getMimeType() {
		return mimeType;
	}

	@JsonProperty("mime_type")
	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	@JsonProperty("file")
	public String getFile() {
		return file;
	}

	@JsonProperty("file")
	public void setFile(String file) {
		this.file = file;
	}

	@JsonProperty("media_type")
	public String getMediaType() {
		return mediaType;
	}

	@JsonProperty("media_type")
	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	@JsonProperty("preview")
	public String getPreview() {
		return preview;
	}

	@JsonProperty("preview")
	public void setPreview(String preview) {
		this.preview = preview;
	}

	@JsonProperty("path")
	public String getPath() {
		return path;
	}

	@JsonProperty("path")
	public void setPath(String path) {
		this.path = path;
	}

	@JsonProperty("type")
	public String getType() {
		return type;
	}

	@JsonProperty("type")
	public void setType(String type) {
		this.type = type;
	}

	@JsonProperty("sha256")
	public String getSha256() {
		return sha256;
	}

	@JsonProperty("sha256")
	public void setSha256(String sha256) {
		this.sha256 = sha256;
	}

	@JsonProperty("md5")
	public String getMd5() {
		return md5;
	}

	@JsonProperty("md5")
	public void setMd5(String md5) {
		this.md5 = md5;
	}
}
