package by.dpi.photoprint_server.controller;

import by.dpi.photoprint_server.model.PhotoItem;
import by.dpi.photoprint_server.service.PhotoService;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.util.Collections;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

@RestController
@RequestMapping(path = "api_v3/photo", produces = {"application/json"})
public class PhotoController {

	final PhotoService photoService;

	PhotoController(PhotoService photoService) {
		this.photoService = photoService;
	}

	@GetMapping
	public SortedSet<PhotoItem> getPhotoList(@RequestParam(required = false) String path) throws InterruptedException {
		return new TreeSet<>(photoService.getPhotoItems(path != null ? path : "/").values());
	}

	@DeleteMapping
	public Map<String, String> deletePhoto(@RequestParam String type, @RequestParam String path, @RequestParam String name) throws InterruptedException {
		return photoService.deletePhotoItem(type, path, name);
	}

	@RequestMapping(value = "/image-thumbnail", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageThumbnail(@RequestParam String path, @RequestParam String name, String size) throws InterruptedException, IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.IMAGE_JPEG);
		headers.setAccept(Collections.singletonList(MediaType.IMAGE_JPEG));
		headers.setCacheControl(CacheControl.maxAge(Duration.ofDays(30)));
		return new ResponseEntity<>(photoService.getImage(path, name, size), headers, HttpStatus.OK);
	}

	@RequestMapping(value = "/image-response", method = RequestMethod.GET)
	public ResponseEntity<byte[]> getImageDownload(@RequestParam String path, @RequestParam String name) throws IOException, InterruptedException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept-Ranges", "bytes");
		headers.setContentType(MediaType.IMAGE_JPEG);
		headers.setAccept(Collections.singletonList(MediaType.IMAGE_JPEG));
		headers.setCacheControl(CacheControl.noCache().getHeaderValue());
		headers.setContentDisposition(
			ContentDisposition.attachment().name("attachment").filename(
				name.replaceAll("^.*/", "") + ".jpeg", StandardCharsets.UTF_8
			).build());
		return new ResponseEntity<>(photoService.getImage(path, name), headers, HttpStatus.OK);
	}
}
