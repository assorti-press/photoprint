package by.dpi.photoprint_server.controller;

import by.dpi.photoprint_server.model.*;
import by.dpi.photoprint_server.service.implementation.PriceFirestoreService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(path = "api_v3/price", produces = {"application/json"})
public class PriceController {

	final PriceFirestoreService firestoreService;

	PriceController(PriceFirestoreService firestoreService) {
		this.firestoreService = firestoreService;
	}

	@GetMapping("/generateDefault")
	public List<PriceList> getListGen() {
		List<PriceList> priceLists = new ArrayList<>() {{
			add(new PriceList(new Quality(0, "Эконом", "economy"), new ArrayList<>() {{
				add(new SizePrice(new Size(10, 15), 0.29d));
				add(new SizePrice(new Size(15, 21), 0.65d));
				add(new SizePrice(new Size(21, 30), 1.25d));
			}}));
			add(new PriceList(new Quality(1, "Стандарт", "standard"), new ArrayList<>() {{
				add(new SizePrice(new Size(10, 15), 0.39d));
				add(new SizePrice(new Size(13, 18), 0.39d));
				add(new SizePrice(new Size(15, 21), 0.75d));
				add(new SizePrice(new Size(21, 30), 1.50d));
			}}));
			add(new PriceList(new Quality(2, "Премиум", "premium"), new ArrayList<>() {{
				add(new SizePrice(new Size(10, 15), 0.39d));
				add(new SizePrice(new Size(13, 18), 1.39d));
				add(new SizePrice(new Size(15, 21), 1.75d));
				add(new SizePrice(new Size(21, 30), 1.50d));
			}}));
			add(new PriceList(new Quality(3, "Суперглянец", "supergloss"), new ArrayList<>() {{
				add(new SizePrice(new Size(10, 15), 2.39d));
				add(new SizePrice(new Size(13, 18), 2.39d));
				add(new SizePrice(new Size(15, 21), 2.75d));
				add(new SizePrice(new Size(21, 30), 2.50d));
			}}));

		}};

		firestoreService.set(priceLists);

		return priceLists;
	}

	@GetMapping
	public List<PriceList> getList() throws ExecutionException, InterruptedException {
		return firestoreService.get();
	}

	@PostMapping
	public void setList(@RequestBody List<PriceList> priceList) {
		if (priceList != null) firestoreService.set(priceList);
	}
}
