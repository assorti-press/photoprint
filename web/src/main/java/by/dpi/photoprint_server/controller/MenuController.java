package by.dpi.photoprint_server.controller;

import by.dpi.photoprint_server.model.MenuItem;
import by.dpi.photoprint_server.service.MenuFirestoreService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(path = "api_v3/menu", produces = {"application/json"})
public class MenuController {

	private final MenuFirestoreService menuFirestoreService;

	MenuController(MenuFirestoreService menuFirestoreService) {
		this.menuFirestoreService = menuFirestoreService;
	}

	List<MenuItem> priceLists = new ArrayList<>() {{
		add(new MenuItem("delivery", "Доставка", "<!DOCTYPE html>\n" +
			"<html lang=\"en\">\n" +
			"    <head>\n" +
			"        <meta charset=\"UTF-8\">\n" +
			"        <title>Delivery</title>\n" +
			"    </head>\n" +
			"    <body>\n" +
			"        <h3>Доставка</h3>\n" +
			"        <div>Доставка осуществляется только по Республике Беларусь.</div>\n" +
			"        <div>Доставка осуществляется <b>во все населённые пункты Республики Беларусь</b>.</div>\n" +
			"        <div>Стоимость доставки не зависит от расстояния и объёма заказа.</div>\n" +
			"        <div>Стоимость доставки фиксированная и составляет <b>3 руб</b>.\n" +
			"        </div>\n" +
			"    </body>\n" +
			"</html>", 0));
		add(new MenuItem("price", "Цена", "<!DOCTYPE html>\n" +
			"<html lang=\"en\">\n" +
			"    <head>\n" +
			"        <meta charset=\"UTF-8\">\n" +
			"        <title>Price</title>\n" +
			"    </head>\n" +
			"    <body>\n" +
			"        <h3>Цены на печать фотографий</h3>\n" +
			"\n" +
			"        <h4>Сатин</h4>\n" +
			"        <table>\n" +
			"            <tbody>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">Размер, см</td>\n" +
			"                    <td style=\"width: 40%;\">Цена, руб.</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">10 x 15 (A6)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.75</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">13 x 18</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">2.00</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">15 x 21 (A5)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">2.00</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">21 x 30 (A4)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">3.50</td>\n" +
			"                </tr>\n" +
			"            </tbody>\n" +
			"        </table>\n" +
			"        <h4>Суперглянец</h4>\n" +
			"        <table>\n" +
			"            <tbody>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">Размер, см</td>\n" +
			"                    <td style=\"width: 40%;\">Цена, руб.</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">10 x 15 (A6)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.75</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">13 x 18</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">2.00</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">15 x 21 (A5)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">2.00</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">21 x 30 (A4)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">3.50</td>\n" +
			"                </tr>\n" +
			"            </tbody>\n" +
			"        </table>\n" +
			"        <h4>Премиум</h4>\n" +
			"        <table>\n" +
			"            <tbody>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">Размер, см</td>\n" +
			"                    <td style=\"width: 40%;\">Цена, руб.</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">10 x 15 (A6)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.49</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">13 x 18</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">1.50</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">15 x 21 (A5)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">1.50</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">21 x 30 (A4)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">2.50</td>\n" +
			"                </tr>\n" +
			"            </tbody>\n" +
			"        </table>\n" +
			"        <h4>Стандарт</h4>\n" +
			"        <table>\n" +
			"            <tbody>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">Размер, см</td>\n" +
			"                    <td style=\"width: 40%;\">Цена, руб.</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">10 x 15 (A6)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.39</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">13 x 18</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.75</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">15 x 21 (A5)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.75</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">21 x 30 (A4)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">1.50</td>\n" +
			"                </tr>\n" +
			"            </tbody>\n" +
			"        </table>\n" +
			"        <h4>Эконом</h4>\n" +
			"        <table>\n" +
			"            <tbody>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">Размер, см</td>\n" +
			"                    <td style=\"width: 40%;\">Цена, руб.</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">10 x 15 (A6)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.29</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">13 x 18</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.75</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">15 x 21 (A5)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">0.65</td>\n" +
			"                </tr>\n" +
			"                <tr>\n" +
			"                    <td style=\"width: 40%;\">21 x 30 (A4)</td>\n" +
			"                    <td style=\"width: 40%; text-align: center;\">1.25</td>\n" +
			"                </tr>\n" +
			"            </tbody>\n" +
			"        </table>\n" +
			"    </body>\n" +
			"</html>\n", 1));
	}};

	@GetMapping("/generateDefault")
	public List<MenuItem> genList() throws ExecutionException, InterruptedException {

		menuFirestoreService.setList(priceLists);
		return priceLists;
	}

	@GetMapping
	public List<MenuItem> getList() throws ExecutionException, InterruptedException {
		return menuFirestoreService.getList();
	}

	@PatchMapping
	public String updateList(@RequestBody List<MenuItem> menuItems) throws ExecutionException, InterruptedException {
		return menuFirestoreService.updateList(menuItems) ? "updated" : "error";
	}

	@GetMapping("/{name}")
	public MenuItem getItem(@PathVariable String name) throws ExecutionException, InterruptedException {
		return menuFirestoreService.getItem(name);
	}

	@DeleteMapping("/{name}")
	public String deleteItem(@PathVariable String name) {
		return menuFirestoreService.deleteItem(name) ? "deleted" : "error";
	}

	@PostMapping("/{name}")
	public String insertItem(@PathVariable String name, @RequestBody MenuItem postMenuItem) throws ExecutionException, InterruptedException {
		return menuFirestoreService.insertItem(name, postMenuItem) ? "inserted" : "error";
	}

	@PatchMapping("/{name}")
	public String updateItem(@PathVariable String name, @RequestBody MenuItem putMenuItem) throws ExecutionException, InterruptedException {
		return menuFirestoreService.updateItem(name, putMenuItem) ? "updated" : "error";
	}
}
