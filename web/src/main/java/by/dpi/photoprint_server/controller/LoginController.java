package by.dpi.photoprint_server.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.CurrentSecurityContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

    @GetMapping
    public String login(@CurrentSecurityContext(expression = "authentication")
                                Authentication authentication) {
        if(!authentication.getName().equals("anonymousUser"))
            return "redirect:/";
        return "login";
    }
}
