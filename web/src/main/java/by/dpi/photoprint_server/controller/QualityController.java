package by.dpi.photoprint_server.controller;

import by.dpi.photoprint_server.model.*;
import by.dpi.photoprint_server.service.implementation.QualityFirestoreService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(path = "api_v3/quality", produces = {"application/json"})
public class QualityController {

	final QualityFirestoreService firestoreService;

	public QualityController(QualityFirestoreService firestoreService) {
		this.firestoreService = firestoreService;
	}

	@GetMapping("/generateDefault")
	public List<Quality> getListGen() {
		List<Quality> qualities = new ArrayList<>() {{
			add(new Quality(0, "Эконом", "economy"));
			add(new Quality(1, "Стандарт", "standard"));
			add(new Quality(2, "Премиум", "premium"));
			add(new Quality(3, "Суперглянец", "supergloss"));
		}};

		firestoreService.set(qualities);

		return qualities;
	}

	@GetMapping
	public List<Quality> getList() throws ExecutionException, InterruptedException {
		return firestoreService.get();
	}
}
