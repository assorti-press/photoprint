package by.dpi.photoprint_server.controller;

import by.dpi.photoprint_server.model.Size;
import by.dpi.photoprint_server.service.implementation.SizeFirestoreService;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(path = "api_v3/size", produces = {"application/json"})
public class SizeController {
	final SizeFirestoreService firestoreService;

	public SizeController(SizeFirestoreService firestoreService) {
		this.firestoreService = firestoreService;
	}

	@GetMapping("/generateDefault")
	public List<Size> getListGen() {
		List<Size> sizes = new ArrayList<>() {{
			add(new Size(10, 15));
			add(new Size(13, 18));
			add(new Size(15, 21));
			add(new Size(21, 30));
		}};

		firestoreService.set(sizes);

		return sizes;
	}

	@GetMapping
	public List<Size> getList() throws ExecutionException, InterruptedException {
		return firestoreService.get();
	}
}
