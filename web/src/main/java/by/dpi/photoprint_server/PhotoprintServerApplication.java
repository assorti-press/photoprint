package by.dpi.photoprint_server;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PhotoprintServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PhotoprintServerApplication.class, args);
	}
}
